package no.nla.nlacritters.ai;

import java.util.EnumSet;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.DamageSource;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public class SentryShootGoal extends Goal {

	private final TF2SentryEntity sentry;
	private int shootTimer = 0;
	
	public SentryShootGoal(TF2SentryEntity sentry) {
		this.sentry = sentry;
		this.setMutexFlags(EnumSet.of(Goal.Flag.LOOK));
	}
	
	@Override
	public boolean shouldExecute() {
		if (this.sentry.isReady())
			if (this.sentry.getAttackTarget() != null)
				return this.sentry.getAttackTarget().isAlive();
		return false;
	}
	
	@Override
	public boolean shouldContinueExecuting() {
		if (this.sentry.getAttackTarget() != null)
			return this.sentry.getAttackTarget().isAlive();
		return false;
	}
	
	@Override
	public boolean isPreemptible() {
		return true;
	}
	
	@Override
	public void tick(){
		
		this.sentry.getLookController().setLookPositionWithEntity(this.sentry.getAttackTarget(), 90.F, 90.F);
		if (this.shootTimer > 0) {
			this.shootTimer --;
		}
		else {
			this.shootTimer = 20 / this.sentry.getLevel();
			if (this.shouldContinueExecuting()) {
				this.sentry.getAttackTarget().attackEntityFrom(DamageSource.GENERIC, 2 * this.sentry.getLevel());
				this.sentry.getAttackTarget().setRevengeTarget(this.sentry);
			}
		}		
	}
}
