package no.nla.nlacritters.ai;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.EntityPredicates;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public class SentryFindTargetGoal extends Goal {

	private final TF2SentryEntity sentry;

	public SentryFindTargetGoal(TF2SentryEntity sentry) {
		this.sentry = sentry;
	}

	@Override
	public boolean shouldExecute() {
		return this.sentry.isReady();
	}

	@Override
	public void tick() {
		List<Entity> entities = this.sentry.world.getEntitiesInAABBexcluding(this.sentry,
				this.sentry.getBoundingBox().grow(12), EntityPredicates.IS_ALIVE);

		if (entities.size() > 0) {
			float shortestDist = Float.MAX_VALUE;
			for (Entity e : entities) {
				if (e instanceof LivingEntity) {
					LivingEntity livingEntity = (LivingEntity) e;
					if (this.sentry.canEntityBeSeen(livingEntity)) {
						if (this.sentry.shouldAttack(livingEntity)) {
							if (this.sentry.getDistance(livingEntity) < shortestDist) {
								shortestDist = this.sentry.getDistance(livingEntity);
								this.sentry.setAttackTarget(livingEntity);
								return;
							}
						}
					}
				}
			}
		}
	}
}
