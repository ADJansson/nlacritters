package no.nla.nlacritters.ai;

import java.util.EnumSet;

import net.minecraft.entity.ai.goal.Goal;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public class SentryIdleGoal extends Goal {

	private final TF2SentryEntity sentry;

	public SentryIdleGoal(TF2SentryEntity sentry) {
		this.sentry = sentry;
		this.setMutexFlags(EnumSet.of(Goal.Flag.LOOK));
	}

	@Override
	public boolean shouldExecute() {
		return this.sentry.isReady();
	}

	@Override
	public boolean isPreemptible() {
		return true;
	}

	@Override
	public void tick() {

		this.sentry.rotationYawHead = this.sentry.rotationYaw + (float) (45.F * Math.sin(this.sentry.ticksExisted * 0.05));
	}
}
