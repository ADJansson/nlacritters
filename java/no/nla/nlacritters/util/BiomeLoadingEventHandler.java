package no.nla.nlacritters.util;

import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import no.nla.nlacritters.entity.ModEntityType;

@Mod.EventBusSubscriber
public class BiomeLoadingEventHandler {

	@SubscribeEvent
	public static void handleBiomeLoadingEvent(BiomeLoadingEvent evt) {
		ModEntityType.addEntitySpawns(evt);
	}
}
