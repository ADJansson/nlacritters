package no.nla.nlacritters.util;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.MobSpawnInfo;

public class NLAMobSpawnInfo {

	private final EntityType<?> entityType;
	private final RegistryKey<Biome> biome;
	private final int spawnProb;
	private final int minSpawn;
	private final int maxSpawn;
	
	public NLAMobSpawnInfo(EntityType<?> entityType, RegistryKey<Biome> biome, int spawnProb, int minSpawn, int maxSpawn) {
		this.entityType = entityType;
		this.biome = biome;
		this.spawnProb = spawnProb;
		this.minSpawn = minSpawn;
		this.maxSpawn = maxSpawn;
	}

	public MobSpawnInfo.Spawners getSpawnInfo(){
		return new MobSpawnInfo.Spawners(this.entityType, this.spawnProb, this.minSpawn, this.maxSpawn);
	}

	public EntityClassification getClassification() {
		return this.entityType.getClassification();
	}
	
	public ResourceLocation getLocation() {
		return biome.getLocation();
	}
}
