package no.nla.nlacritters.util;

public class ModConstants {

	public static final int GIRAFFE_SKINS = 1;
	public static final int GORILLA_SKINS = 5;
	public static final int ORANGUTAN_SKINS = 1;
	public static final int REINDEER_SKINS = 3;
	public static final int SCORPION_SKINS = 4;
	public static final int SEAHORSE_SKINS = 3;
}
