package no.nla.nlacritters.entity;

import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.controller.DolphinLookController;
import net.minecraft.entity.ai.controller.MovementController;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.ai.goal.FollowBoatGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.ai.goal.RandomSwimmingGoal;
import net.minecraft.entity.monster.DrownedEntity;
import net.minecraft.entity.monster.GuardianEntity;
import net.minecraft.entity.passive.WaterMobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.pathfinding.SwimmerPathNavigator;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import no.nla.nlacritters.util.ModConstants;
import no.nla.nlacritters.util.ModTags;

public class SeahorseEntity extends WaterMobEntity {

	private static final DataParameter<Integer> SKIN_INDEX = EntityDataManager.<Integer>createKey(SeahorseEntity.class,
			DataSerializers.VARINT);

	protected SeahorseEntity(EntityType<? extends WaterMobEntity> type, World p_i48565_2_) {
		super(type, p_i48565_2_);
		this.moveController = new SeahorseEntity.MoveHelperController(this);
		this.lookController = new DolphinLookController(this, 10);
		// SHOO BEE DOO MOTHERBUCKER
	}

	@Nullable
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason,
			@Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
		this.dataManager.set(SKIN_INDEX, worldIn.getRandom().nextInt(ModConstants.SEAHORSE_SKINS));
	      this.rotationPitch = 0.0F;
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	public static AttributeModifierMap.MutableAttribute prepareAttributes() {
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 24.0D)
				.createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.15D);
	}

	protected void registerData() {
		super.registerData();
		this.dataManager.register(SKIN_INDEX, Integer.valueOf(0));
	}

	protected PathNavigator createNavigator(World worldIn) {
		return new SwimmerPathNavigator(this, worldIn);
	}

	protected void registerGoals() {
		this.goalSelector.addGoal(0, new PanicGoal(this, 1.0D));
		this.goalSelector.addGoal(1, new RandomSwimmingGoal(this, 1.0D, 55));
		this.goalSelector.addGoal(4, new LookRandomlyGoal(this));
		this.goalSelector.addGoal(5, new LookAtGoal(this, PlayerEntity.class, 6.0F));
		this.goalSelector.addGoal(8, new FollowBoatGoal(this));
		this.goalSelector.addGoal(9, new AvoidEntityGoal<>(this, GuardianEntity.class, 8.0F, 1.0D, 1.0D));
		this.goalSelector.addGoal(9, new AvoidEntityGoal<>(this, DrownedEntity.class, 8.0F, 1.0D, 1.0D));
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		compound.putInt(ModTags.SKIN_INDEX, this.dataManager.get(SKIN_INDEX));
		super.writeAdditional(compound);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		this.dataManager.set(SKIN_INDEX, compound.getInt(ModTags.SKIN_INDEX));
		super.readAdditional(compound);
	}

	@Override
	public void travel(Vector3d travelVector) {
		if (this.isServerWorld() && this.isInWater()) {
			this.moveRelative(this.getAIMoveSpeed(), travelVector);
			this.move(MoverType.SELF, this.getMotion());
			this.setMotion(this.getMotion().scale(0.9D));
			if (this.getAttackTarget() == null) {
				this.setMotion(this.getMotion().add(0.0D, -0.005D, 0.0D));
			}
		} else {
			super.travel(travelVector);
		}
	}

   public static boolean canSpawn(EntityType<SeahorseEntity> type, IWorld worldIn, SpawnReason reason, BlockPos p_223363_3_, Random randomIn) {
	      return worldIn.getBlockState(p_223363_3_).isIn(Blocks.WATER) && worldIn.getBlockState(p_223363_3_.up()).isIn(Blocks.WATER);
	   }
	
	public boolean canBeLeashedTo(PlayerEntity player) {
		return true;
	}

	public int getVerticalFaceSpeed() {
		return 1;
	}

	public int getHorizontalFaceSpeed() {
		return 1;
	}

	protected boolean canBeRidden(Entity entityIn) {
		return true;
	}

	public boolean isSaddled() {
		return false;
	}

	public void tick() {
		super.tick();
	}

	public int getSkinIndex() {
		return this.dataManager.get(SKIN_INDEX);
	}

	static class MoveHelperController extends MovementController {
		private final SeahorseEntity seahorse;

		public MoveHelperController(SeahorseEntity seahorseIn) {
			super(seahorseIn);
			this.seahorse = seahorseIn;
		}

		public void tick() {
			if (this.seahorse.isInWater()) {
				this.seahorse.setMotion(this.seahorse.getMotion().add(0.0D, 0.005D, 0.0D));
			}
			double d0 = this.posX - this.seahorse.getPosX();
			double d1 = this.posY - this.seahorse.getPosY();
			double d2 = this.posZ - this.seahorse.getPosZ();
			double d3 = d0 * d0 + d1 * d1 + d2 * d2;
			if (d3 < (double) 2.5000003E-7F) {
				this.mob.setMoveForward(0.0F);
			} else {
				float f = (float) (MathHelper.atan2(d2, d0) * (double) (180F / (float) Math.PI)) - 90.0F;
				this.seahorse.rotationYaw = this.limitAngle(this.seahorse.rotationYaw, f, 10.0F);
				this.seahorse.renderYawOffset = this.seahorse.rotationYaw;
				this.seahorse.rotationYawHead = this.seahorse.rotationYaw;
				float f1 = (float) (this.speed * this.seahorse.getAttributeValue(Attributes.MOVEMENT_SPEED));
				if (this.seahorse.isInWater()) {
					this.seahorse.setAIMoveSpeed(f1 * 0.02F);
					float f2 = -((float) (MathHelper.atan2(d1, (double) MathHelper.sqrt(d0 * d0 + d2 * d2))
							* (double) (180F / (float) Math.PI)));
					f2 = MathHelper.clamp(MathHelper.wrapDegrees(f2), -85.0F, 85.0F);
					this.seahorse.rotationPitch = this.limitAngle(this.seahorse.rotationPitch, f2, 5.0F);
					float f3 = MathHelper.cos(this.seahorse.rotationPitch * ((float) Math.PI / 180F));
					float f4 = MathHelper.sin(this.seahorse.rotationPitch * ((float) Math.PI / 180F));
					this.seahorse.moveForward = f3 * f1;
					this.seahorse.moveVertical = -f4 * f1;
				} else {
					this.seahorse.setAIMoveSpeed(f1 * 0.1F);
				}
			}
			
			super.tick();
		}
	}
}
