package no.nla.nlacritters.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.BreedGoal;
import net.minecraft.entity.ai.goal.EatGrassGoal;
import net.minecraft.entity.ai.goal.FollowParentGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.TemptGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import no.nla.nlacritters.util.ModConstants;
import no.nla.nlacritters.util.ModTags;

public class ReindeerEntity extends AnimalEntity {

	private static final DataParameter<Integer> SKIN_INDEX = EntityDataManager.<Integer>createKey(ReindeerEntity.class,
			DataSerializers.VARINT);

	private int reindeerTimer;
	private EatGrassGoal eatGrassGoal;

	public ReindeerEntity(EntityType<? extends AnimalEntity> type, World worldIn) {
		super(type, worldIn);
	}

	@Nullable
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason,
			@Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
		this.dataManager.set(SKIN_INDEX, worldIn.getRandom().nextInt(ModConstants.REINDEER_SKINS));
		return spawnDataIn;
	}

	protected void updateAITasks() {
		this.reindeerTimer = this.eatGrassGoal.getEatingGrassTimer();
		super.updateAITasks();
	}

	public void livingTick() {
		if (this.world.isRemote) {
			this.reindeerTimer = Math.max(0, this.reindeerTimer - 1);
		}

		super.livingTick();
	}

	public static AttributeModifierMap.MutableAttribute prepareAttributes() {
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 18.0D)
				.createMutableAttribute(Attributes.MOVEMENT_SPEED, (double) 0.35F);
	}

	protected void registerGoals() {
		this.eatGrassGoal = new EatGrassGoal(this);

		this.goalSelector.addGoal(1, new SwimGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 2.2D));
		this.goalSelector.addGoal(2, new BreedGoal(this, 1.0D));
		this.goalSelector.addGoal(3, new TemptGoal(this, 1.25D, Ingredient.fromItems(Items.WHEAT), false));
		this.goalSelector.addGoal(3, new TemptGoal(this, 1.25D, Ingredient.fromItems(Items.CARROT), false));
		this.goalSelector.addGoal(4, new FollowParentGoal(this, 1.25D));
		this.goalSelector.addGoal(5, this.eatGrassGoal);
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.8D));
		this.goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new LookRandomlyGoal(this));

		// new EatGrassGoal(null)
	}

	protected void registerData() {
		super.registerData();
		this.dataManager.register(SKIN_INDEX, Integer.valueOf(0));
	}

	/**
	 * Handler for {@link World#setEntityState}
	 */
	@OnlyIn(Dist.CLIENT)
	public void handleStatusUpdate(byte id) {
		if (id == 10) {
			this.reindeerTimer = 40;
		} else {
			super.handleStatusUpdate(id);
		}

	}

	@OnlyIn(Dist.CLIENT)
	public float getHeadRotationPointY(float p_70894_1_) {
		if (this.reindeerTimer <= 0) {
			return 0.0F;
		} else if (this.reindeerTimer >= 4 && this.reindeerTimer <= 36) {
			return 1.0F;
		} else {
			return this.reindeerTimer < 4 ? ((float) this.reindeerTimer - p_70894_1_) / 4.0F
					: -((float) (this.reindeerTimer - 40) - p_70894_1_) / 4.0F;
		}
	}

	@OnlyIn(Dist.CLIENT)
	public float getHeadRotationAngleX(float p_70890_1_) {
		if (this.reindeerTimer > 4 && this.reindeerTimer <= 36) {
			float f = ((float) (this.reindeerTimer - 4) - p_70890_1_) / 32.0F;
			return ((float) Math.PI / 5F) + 0.21991149F * MathHelper.sin(f * 28.7F);
		} else {
			return this.reindeerTimer > 0 ? ((float) Math.PI / 5F) : this.rotationPitch * ((float) Math.PI / 180F);
		}
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		compound.putInt(ModTags.SKIN_INDEX, this.dataManager.get(SKIN_INDEX));
		super.writeAdditional(compound);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		this.dataManager.set(SKIN_INDEX, compound.getInt(ModTags.SKIN_INDEX));
		super.readAdditional(compound);
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn) {
		return this.isChild() ? 0.9F : 1.9F;
	}

	public boolean isBreedingItem(ItemStack stack) {
		return stack.getItem() == Items.WHEAT || stack.getItem() == Items.CARROT;
	}

	public int getSkinIndex() {
		return this.dataManager.get(SKIN_INDEX);
	}
	
	protected void setSkinIndex(int index) {
		if (index < ModConstants.REINDEER_SKINS) {
			this.dataManager.set(SKIN_INDEX, index);
		}
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld p_241840_1_, AgeableEntity p_241840_2_) {
		// babby
		ReindeerEntity babby = new ReindeerEntity(ModEntityType.REINDEER, p_241840_1_);
		babby.setSkinIndex(this.getSkinIndex());
		return babby;
	}
}
