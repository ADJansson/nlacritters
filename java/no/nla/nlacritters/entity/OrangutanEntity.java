package no.nla.nlacritters.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import no.nla.nlacritters.client.NLASoundEvents;
import no.nla.nlacritters.util.ModConstants;

public class OrangutanEntity extends AbstractMonkeyEntity {

	public OrangutanEntity(EntityType<? extends AbstractMonkeyEntity> type, World worldIn) {
		super(type, worldIn);
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld p_241840_1_, AgeableEntity p_241840_2_) {
		return new OrangutanEntity(ModEntityType.ORANGUTAN, p_241840_1_);
	}
	
	@Nullable
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason,
			@Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
		this.dataManager.set(SKIN_INDEX, worldIn.getRandom().nextInt(ModConstants.ORANGUTAN_SKINS));
		return spawnDataIn;
	}
	
	protected SoundEvent getAmbientSound() {
		return NLASoundEvents.ENTITY_ORANGUTAN_AMBIENT;
	}
	
	public static AttributeModifierMap.MutableAttribute prepareAttributes() {
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 16.0D)
				.createMutableAttribute(Attributes.MOVEMENT_SPEED, (double) 0.3F);
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn) {
		return this.isChild() ? 0.6F : 1.2F;
	}
}
