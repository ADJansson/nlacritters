package no.nla.nlacritters.entity.projectile;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.DamagingProjectileEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.Difficulty;
import net.minecraft.world.World;

public class SentryBulletEntity extends DamagingProjectileEntity {

	private LivingEntity shooter;

	public SentryBulletEntity(EntityType<? extends SentryBulletEntity> p_i50160_1_, World p_i50160_2_) {
		super(p_i50160_1_, p_i50160_2_);
	}

	public void init(World worldIn, LivingEntity shooter, double accelX, double accelY, double accelZ) {
		this.setShooter(shooter);
		this.setLocationAndAngles(shooter.getPosX(), shooter.getPosY(), shooter.getPosZ(), this.rotationYaw,
				this.rotationPitch);
		this.setRotation(shooter.rotationYaw, shooter.rotationPitch);
		this.recenterBoundingBox();
		double d0 = (double) MathHelper.sqrt(accelX * accelX + accelY * accelY + accelZ * accelZ);
		if (d0 != 0.0D) {
			this.accelerationX = accelX / d0 * 0.1D;
			this.accelerationY = accelY / d0 * 0.1D;
			this.accelerationZ = accelZ / d0 * 0.1D;
		}
	}

	public void setShooter(LivingEntity entityIn) {
		super.setShooter(entityIn);
		this.shooter = entityIn;
	}

	protected void onEntityHit(EntityRayTraceResult p_213868_1_) {
		super.onEntityHit(p_213868_1_);
		if (!this.world.isRemote) {
			Entity entity = p_213868_1_.getEntity();
			entity.attackEntityFrom(DamageSource.causeMobDamage(this.shooter), 2.0F);
		}
	}

	protected boolean isFireballFiery() {
		return false;
	}

	public boolean isBurning() {
		return false;
	}

	public boolean canBeCollidedWith() {
		return false;
	}

	public boolean attackEntityFrom(DamageSource source, float amount) {
		return false;
	}
}
