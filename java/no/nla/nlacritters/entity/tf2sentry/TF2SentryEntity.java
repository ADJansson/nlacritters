package no.nla.nlacritters.entity.tf2sentry;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.PistonBlock;
import net.minecraft.block.PistonHeadBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.controller.BodyController;
import net.minecraft.entity.monster.AbstractSkeletonEntity;
import net.minecraft.entity.monster.SpiderEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.passive.GolemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ShulkerAABBHelper;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import no.nla.nlacritters.ai.SentryFindTargetGoal;
import no.nla.nlacritters.ai.SentryIdleGoal;
import no.nla.nlacritters.ai.SentryShootGoal;
import no.nla.nlacritters.entity.ModEntityType;
import no.nla.nlacritters.util.ModTags;

public class TF2SentryEntity extends GolemEntity {

	private static final DataParameter<Integer> TEAM_COLOR = EntityDataManager.<Integer>createKey(TF2SentryEntity.class,
			DataSerializers.VARINT);

	protected static final DataParameter<Direction> ATTACHED_FACE = EntityDataManager.createKey(TF2SentryEntity.class,
			DataSerializers.DIRECTION);
	protected static final DataParameter<Optional<BlockPos>> ATTACHED_BLOCK_POS = EntityDataManager
			.createKey(TF2SentryEntity.class, DataSerializers.OPTIONAL_BLOCK_POS);
	private static final DataParameter<Float> YAW_VAL = EntityDataManager.<Float>createKey(TF2SentryEntity.class,
			DataSerializers.FLOAT);
	private static final DataParameter<Integer> BUILD_TIMER = EntityDataManager.<Integer>createKey(TF2SentryEntity.class,
			DataSerializers.VARINT);
	
	//private int buildTimer = -1;
	public float tripodBuildOffsetY = 0.F;
	public float tripodBuildRotation = 0.F;
	public float turretBuildRotation = 0.F;
	private int upgradeProgress = 0;
	
	public TF2SentryEntity(EntityType<? extends TF2SentryEntity> type, World worldIn) {
		super(type, worldIn);
	}

	protected void registerData() {
		super.registerData();
		this.dataManager.register(TEAM_COLOR, Integer.valueOf(0));
		this.dataManager.register(ATTACHED_FACE, Direction.DOWN);
		this.dataManager.register(ATTACHED_BLOCK_POS, Optional.empty());
		this.dataManager.register(YAW_VAL, Float.valueOf(0));
		this.dataManager.register(BUILD_TIMER, Integer.valueOf(-1));
	}

	public static AttributeModifierMap.MutableAttribute prepareAttributes() {
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 3.0D)
				.createMutableAttribute(Attributes.MOVEMENT_SPEED, (double) 0.F);
	}

	protected void registerGoals() {
		this.goalSelector.addGoal(0, new SentryShootGoal(this));
		this.goalSelector.addGoal(0, new SentryFindTargetGoal(this));
		this.goalSelector.addGoal(1, new SentryIdleGoal(this));
	}

	protected boolean canTriggerWalking() {
		return false;
	}
	
	public boolean shouldAttack(LivingEntity entity) {
		
		return entity instanceof ZombieEntity || entity instanceof AbstractSkeletonEntity || entity instanceof SpiderEntity;
	}

	@Override
	public boolean canBePushed() {
		return super.canBePushed();
	}

	protected BodyController createBodyController() {
		return new TF2SentryEntity.BodyHelperController(this);
	}

	public void livingTick() {
		super.livingTick();
	}

	public void tick() {
		super.tick();

		BlockPos blockpos = this.dataManager.get(ATTACHED_BLOCK_POS).orElse((BlockPos) null);
			
		if (!this.isOnGround())
			blockpos = null;
		
		if (blockpos == null && !this.world.isRemote) {
			blockpos = this.getPosition();
			this.dataManager.set(ATTACHED_BLOCK_POS, Optional.of(blockpos));
		}

		if (this.isPassenger()) {
			blockpos = null;
			float f = this.getRidingEntity().rotationYaw;
			this.rotationYaw = f;
			this.renderYawOffset = f;
			this.prevRenderYawOffset = f;
		} else if (!this.world.isRemote) {
			BlockState blockstate = this.world.getBlockState(blockpos);
			if (!(blockstate.getMaterial() == Material.AIR)) {
				if (blockstate.isIn(Blocks.MOVING_PISTON)) {
					Direction direction = blockstate.get(PistonBlock.FACING);
					if (this.world.isAirBlock(blockpos.offset(direction))) {
						blockpos = blockpos.offset(direction);
						this.dataManager.set(ATTACHED_BLOCK_POS, Optional.of(blockpos));
					}
				} else if (blockstate.isIn(Blocks.PISTON_HEAD)) {
					Direction direction3 = blockstate.get(PistonHeadBlock.FACING);
					if (this.world.isAirBlock(blockpos.offset(direction3))) {
						blockpos = blockpos.offset(direction3);
						this.dataManager.set(ATTACHED_BLOCK_POS, Optional.of(blockpos));
					}
				}
			}

			Direction direction4 = this.getAttachmentFacing();
			if (!this.func_234298_a_(blockpos, direction4)) {
				Direction direction1 = this.func_234299_g_(blockpos);
				if (direction1 != null) {
					this.dataManager.set(ATTACHED_FACE, direction1);
				}
			}
		}
		
		this.renderYawOffset = this.dataManager.get(YAW_VAL);
		
		this.tickBuildAnimation();
		
		//System.out.println("build: " + this.buildTimer);
		
		if (blockpos != null) {
			this.forceSetPosition((double) blockpos.getX() + 0.5D, (double) blockpos.getY(),
					(double) blockpos.getZ() + 0.5D);
			double d2 = 0.5D - (double) MathHelper.sin((0.5F) * (float) Math.PI) * 0.5D;
			double d0 = 0.5D - (double) MathHelper.sin((0.5F) * (float) Math.PI) * 0.5D;
			if (this.isAddedToWorld() && this.world instanceof net.minecraft.world.server.ServerWorld)
				((net.minecraft.world.server.ServerWorld) this.world).chunkCheck(this); // Forge - Process chunk
																						// registration after moving.
			Direction direction2 = this.getAttachmentFacing().getOpposite();
			this.setBoundingBox(
					(new AxisAlignedBB(this.getPosX() - 0.5D, this.getPosY(), this.getPosZ() - 0.5D, this.getPosX() + 0.5D,
							this.getPosY() + 1.0D, this.getPosZ() + 0.5D)).expand((double) direction2.getXOffset() * d2,
									(double) direction2.getYOffset() * d2, (double) direction2.getZOffset() * d2));
			double d1 = d2 - d0;
			if (d1 > 0.0D) {
				List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this, this.getBoundingBox());
				if (!list.isEmpty()) {
					for (Entity entity : list) {
						if (!(entity instanceof TF2SentryEntity) && !entity.noClip) {
							entity.move(MoverType.SHULKER, new Vector3d(d1 * (double) direction2.getXOffset(),
									d1 * (double) direction2.getYOffset(), d1 * (double) direction2.getZOffset()));
						}
					}
				}
			}
		}
		
		//this.rotationYawHead += 7.4F;
		 
	}
	
	private void tickBuildAnimation() {
		
		int buildTimer = this.dataManager.get(BUILD_TIMER);
		
		if (buildTimer > 0) {		
			if (buildTimer > 25) {
				this.turretBuildRotation = (float) (Math.PI / 2.D);
				this.tripodBuildOffsetY = ((buildTimer - 25) / 95.F) * 7.F;
				this.tripodBuildRotation = (1 - (buildTimer - 25) / 95.F) * 18.9F;
			} else {
				this.turretBuildRotation = (float) ((buildTimer / 25.F) * Math.PI / 2);
			}
			buildTimer--;
		}		
		this.dataManager.set(BUILD_TIMER, buildTimer);
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if (source.getTrueSource() instanceof PlayerEntity) {
			PlayerEntity player = (PlayerEntity)source.getTrueSource();
			
			if (ItemStack.EMPTY.equals(player.getHeldItemMainhand())) {
				int buildTimer = this.getBuildTimer();
				if (buildTimer > 0) {
					buildTimer -= 7;
					if (buildTimer < 0)
						buildTimer = 0;
					this.dataManager.set(BUILD_TIMER, buildTimer);
					return false;
				}
			} else if (Items.IRON_INGOT.equals(player.getHeldItemMainhand().getItem())) {
				int sC = player.getHeldItemMainhand().getCount() >= 8 ? 8 : player.getHeldItemMainhand().getCount();
				this.upgradeProgress += sC;
				
				System.out.println("h1: " + this.getHealth());
				
				if (this.getHealth() < this.getMaxHealth()) {
					this.heal(sC);
					System.out.println("h2: " + this.getHealth());
				}
				else if (this.upgradeProgress >= 64) {
					if (!this.world.isRemote) {
						this.remove();
						EntityType<? extends TF2SentryEntity> sentryType = ModEntityType.TF2_SENTRY_LVL2;
						TF2SentryEntity newSentry = sentryType.spawn((ServerWorld) world, null, null, player, this.getPosition(), SpawnReason.SPAWN_EGG, true, true);			
						if (newSentry != null) {
							newSentry.setTeamColor(this.getTeamColor());
							newSentry.setYaw(this.rotationYaw);
							newSentry.startBuildTimer();
						}
					}
				}
				//if (!player.isCreative())
				player.getHeldItemMainhand().shrink(sC);
				return false;
			}
		}
		
		return super.attackEntityFrom(source, amount);
	}
	
	public void applyEntityCollision(Entity entityIn) {
	}

	public float getCollisionBorderSize() {
		return 0.0F;
	}
	
	public TF2SentryEntity getLevelUp(ServerWorld worldIn) {
		TF2SentryLvl2Entity newSentry = new TF2SentryLvl2Entity(ModEntityType.TF2_SENTRY_LVL2, worldIn);
		newSentry.setTeamColor(this.getTeamColor());
		newSentry.setYaw(this.dataManager.get(YAW_VAL));
		newSentry.setAttachmentPos(this.dataManager.get(ATTACHED_BLOCK_POS).orElse((BlockPos) null));
		return newSentry;
	}

	public void setYaw(float yaw) {
		this.dataManager.set(YAW_VAL, yaw);
		this.rotationYaw = yaw;
		this.rotationYawHead = yaw;
	}
	
	public float getYaw(float partialTicks) {
		return super.getYaw(partialTicks);
	}
	
	@Override
	public void travel(Vector3d travelVector) {
		// super.travel(new Vector3d(0, travelVector.y, 0));
		super.travel(travelVector);
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		compound.putInt(ModTags.SKIN_INDEX, this.dataManager.get(TEAM_COLOR));
		compound.putByte("AttachFace", (byte) this.dataManager.get(ATTACHED_FACE).getIndex());
		BlockPos blockpos = this.getAttachmentPos();
		if (blockpos != null) {
			compound.putInt("APX", blockpos.getX());
			compound.putInt("APY", blockpos.getY());
			compound.putInt("APZ", blockpos.getZ());
		}
		compound.putFloat("yawVal", this.dataManager.get(YAW_VAL));
		compound.putInt("buildTimer", this.dataManager.get(BUILD_TIMER));
		compound.putInt("upgradeProgress", this.upgradeProgress);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		this.dataManager.set(TEAM_COLOR, compound.getInt(ModTags.SKIN_INDEX));
		this.dataManager.set(ATTACHED_FACE, Direction.byIndex(compound.getByte("AttachFace")));
		if (compound.contains("APX")) {
			int i = compound.getInt("APX");
			int j = compound.getInt("APY");
			int k = compound.getInt("APZ");
			this.dataManager.set(ATTACHED_BLOCK_POS, Optional.of(new BlockPos(i, j, k)));
		} else {
			this.dataManager.set(ATTACHED_BLOCK_POS, Optional.empty());
		}
		this.dataManager.set(YAW_VAL, compound.getFloat("yawVal"));
		this.dataManager.set(BUILD_TIMER, compound.getInt("buildTimer"));
		this.upgradeProgress = compound.getInt("upgradeProgress");
	}

	public Direction getAttachmentFacing() {
		return this.dataManager.get(ATTACHED_FACE);
	}

	@Nullable
	public BlockPos getAttachmentPos() {
		return this.dataManager.get(ATTACHED_BLOCK_POS).orElse((BlockPos) null);
	}

	public void setAttachmentPos(@Nullable BlockPos pos) {
		this.dataManager.set(ATTACHED_BLOCK_POS, Optional.ofNullable(pos));
	}

	public void setTeamColor(int teamColor) {
		this.dataManager.set(TEAM_COLOR, teamColor);
	}

	public int getTeamColor() {
		return this.dataManager.get(TEAM_COLOR);
	}
	
	public int getBuildTimer() {
		return this.dataManager.get(BUILD_TIMER);
	}
	
	public void startBuildTimer() {
		this.dataManager.set(BUILD_TIMER, 120);
	}
	
	public boolean isReady() {
		return this.dataManager.get(BUILD_TIMER) == 0;
	}
	
	public int getLevel() {
		return 1;
	}

	@Nullable
	protected Direction func_234299_g_(BlockPos p_234299_1_) {
		for (Direction direction : Direction.values()) {
			if (this.func_234298_a_(p_234299_1_, direction)) {
				return direction;
			}
		}

		return null;
	}

	private boolean func_234298_a_(BlockPos p_234298_1_, Direction p_234298_2_) {
		return this.world.isDirectionSolid(p_234298_1_.offset(p_234298_2_), this, p_234298_2_.getOpposite())
				&& this.world.hasNoCollisions(this,
						ShulkerAABBHelper.getOpenedCollisionBox(p_234298_1_, p_234298_2_.getOpposite()));
	}

	class BodyHelperController extends BodyController {
		public BodyHelperController(MobEntity p_i50612_2_) {
			super(p_i50612_2_);
		}

		/**
		 * Update the Head and Body rendenring angles
		 */
		public void updateRenderAngles() {
		}
	}
}
