package no.nla.nlacritters.entity.tf2sentry;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.world.World;

public class TF2SentryLvl2Entity extends TF2SentryEntity {

	public TF2SentryLvl2Entity(EntityType<? extends TF2SentryLvl2Entity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public static AttributeModifierMap.MutableAttribute prepareAttributes() {
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 3.0D)
				.createMutableAttribute(Attributes.MOVEMENT_SPEED, (double) 0.F);
	}
	
	@Override
	public int getLevel() {
		return 2;
	}
}
