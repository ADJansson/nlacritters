package no.nla.nlacritters.entity;

import java.util.ArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.projectile.SentryBulletEntity;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryLvl2Entity;
import no.nla.nlacritters.util.NLAMobSpawnInfo;

public class ModEntityType {
	
	public static EntityType<GiraffeEntity> GIRAFFE = make("giraffe", EntityType.Builder.create(GiraffeEntity::new, EntityClassification.CREATURE).size(1.2F, 4.F));
	public static EntityType<GorillaEntity> GORILLA = make("gorilla", EntityType.Builder.create(GorillaEntity::new, EntityClassification.CREATURE).size(1.2F, 1.5F));
	public static EntityType<OrangutanEntity> ORANGUTAN = make("orangutan", EntityType.Builder.create(OrangutanEntity::new, EntityClassification.CREATURE).size(1.0F, 1.1F));
	public static EntityType<ReindeerEntity> REINDEER = make("reindeer", EntityType.Builder.create(ReindeerEntity::new, EntityClassification.CREATURE).size(1.2F, 2.2F));
	public static EntityType<ScorpionEntity> SCORPION = make("scorpion", EntityType.Builder.create(ScorpionEntity::new, EntityClassification.MONSTER).size(1.4F, 0.8F));
	public static EntityType<SeahorseEntity> SEAHORSE = make("seahorse", EntityType.Builder.create(SeahorseEntity::new, EntityClassification.WATER_CREATURE).size(1.6F, 1.5F));
	
	public static EntityType<TF2SentryEntity> TF2_SENTRY_LVL1 = make("tf2sentrylvl1", EntityType.Builder.create(TF2SentryEntity::new, EntityClassification.AMBIENT).size(1.F, 1.F));
	public static EntityType<TF2SentryLvl2Entity> TF2_SENTRY_LVL2 = make("tf2sentrylvl2", EntityType.Builder.create(TF2SentryLvl2Entity::new, EntityClassification.AMBIENT).size(1.F, 1.2F));
	public static EntityType<SentryBulletEntity> SENTRY_BULLET = make("sentry_bullet", EntityType.Builder.create(SentryBulletEntity::new, EntityClassification.MISC).size(.1F, .1F));
	
	private final static ArrayList<NLAMobSpawnInfo> SPAWN_LIST = new ArrayList<NLAMobSpawnInfo>();
	
	public static void registerEntityTypes(final RegistryEvent.Register<EntityType<?>> event){
		event.getRegistry().register(GIRAFFE);
		event.getRegistry().register(GORILLA);	
		event.getRegistry().register(ORANGUTAN);
		event.getRegistry().register(REINDEER);	
		event.getRegistry().register(SCORPION);	
		event.getRegistry().register(SEAHORSE);
		event.getRegistry().register(TF2_SENTRY_LVL1);
		event.getRegistry().register(TF2_SENTRY_LVL2);
	}
	
	private static <T extends Entity> EntityType<T> make(String name, EntityType.Builder<T> builder) {
        EntityType<T> entityType = builder.build(name);
        entityType.setRegistryName(Main.MODID, name);
        return entityType;
    }
	
	public static void registerEggs(final RegistryEvent.Register<Item> event) {
		event.getRegistry().register(makeSpawnEgg(GIRAFFE, 0xBD9A72, 0x472C18, "giraffe"));
		event.getRegistry().register(makeSpawnEgg(GORILLA, 0x472C18, 0xFF00DC, "gorilla"));
		event.getRegistry().register(makeSpawnEgg(ORANGUTAN, 0x7F0000, 0x472C18, "orangutan"));
		event.getRegistry().register(makeSpawnEgg(REINDEER, 0xC0C0C0, 0x7F3300, "reindeer"));
		event.getRegistry().register(makeSpawnEgg(SCORPION, 0x57007F, 0x21007F, "scorpion"));
		event.getRegistry().register(makeSpawnEgg(SEAHORSE, 0x0094FF, 0x00137F, "seahorse"));
		
		//event.getRegistry().register(makeSpawnEgg(TF2_SENTRY_LVL1, 0x000000, 0x111111, "tf2sentry"));
		
		EntitySpawnPlacementRegistry.register(GIRAFFE, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, AnimalEntity::canAnimalSpawn);
		SPAWN_LIST.add(new NLAMobSpawnInfo(GIRAFFE, Biomes.SAVANNA, 12, 1, 6));
		SPAWN_LIST.add(new NLAMobSpawnInfo(GIRAFFE, Biomes.SAVANNA_PLATEAU, 12, 1, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(GIRAFFE, Biomes.SHATTERED_SAVANNA, 12, 1, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(GIRAFFE, Biomes.SHATTERED_SAVANNA_PLATEAU, 12, 1, 4));
		
		EntitySpawnPlacementRegistry.register(GORILLA, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, AnimalEntity::canAnimalSpawn);
		SPAWN_LIST.add(new NLAMobSpawnInfo(GORILLA, Biomes.JUNGLE, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(GORILLA, Biomes.JUNGLE_HILLS, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(GORILLA, Biomes.MODIFIED_JUNGLE, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(GORILLA, Biomes.BAMBOO_JUNGLE, 12, 2, 4));		
		SPAWN_LIST.add(new NLAMobSpawnInfo(GORILLA, Biomes.BAMBOO_JUNGLE_HILLS, 12, 2, 4));
		
		EntitySpawnPlacementRegistry.register(ORANGUTAN, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, AnimalEntity::canAnimalSpawn);
		SPAWN_LIST.add(new NLAMobSpawnInfo(ORANGUTAN, Biomes.JUNGLE, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(ORANGUTAN, Biomes.JUNGLE_HILLS, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(ORANGUTAN, Biomes.MODIFIED_JUNGLE, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(ORANGUTAN, Biomes.BAMBOO_JUNGLE, 12, 2, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(ORANGUTAN, Biomes.BAMBOO_JUNGLE_HILLS, 12, 2, 4));
		
		EntitySpawnPlacementRegistry.register(REINDEER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, AnimalEntity::canAnimalSpawn);
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.ICE_SPIKES, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.TAIGA, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.TAIGA_HILLS, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.TAIGA_MOUNTAINS, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.SNOWY_MOUNTAINS, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.SNOWY_TAIGA, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.SNOWY_TAIGA_HILLS, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.SNOWY_TAIGA_MOUNTAINS, 6, 3, 8));
		SPAWN_LIST.add(new NLAMobSpawnInfo(REINDEER, Biomes.SNOWY_TUNDRA, 6, 3, 8));
		
		EntitySpawnPlacementRegistry.register(SCORPION, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.BADLANDS, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.BADLANDS_PLATEAU, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.ERODED_BADLANDS, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.MODIFIED_BADLANDS_PLATEAU, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.MODIFIED_WOODED_BADLANDS_PLATEAU, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.WOODED_BADLANDS_PLATEAU, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.DESERT, 40, 1, 3));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SCORPION, Biomes.DESERT_HILLS, 40, 1, 3));
		
		EntitySpawnPlacementRegistry.register(SEAHORSE, EntitySpawnPlacementRegistry.PlacementType.IN_WATER, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, SeahorseEntity::canSpawn);
		SPAWN_LIST.add(new NLAMobSpawnInfo(SEAHORSE, Biomes.DEEP_OCEAN, 5, 1, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SEAHORSE, Biomes.DEEP_LUKEWARM_OCEAN, 5, 1, 4));
		SPAWN_LIST.add(new NLAMobSpawnInfo(SEAHORSE, Biomes.DEEP_WARM_OCEAN, 5, 1, 4));
		
	}

	private static Item makeSpawnEgg(EntityType<?> type, int color1, int color2, String name) {
		return new SpawnEggItem(type, color1, color2, new Item.Properties().group(ItemGroup.MISC))
				.setRegistryName(Main.MODID, "spawn_egg." + name);
	}
	
	public static void addEntitySpawns(BiomeLoadingEvent event) {	
		for (NLAMobSpawnInfo mobspawn : SPAWN_LIST) {
			if (event.getName().equals(mobspawn.getLocation()))
				event.getSpawns().getSpawner(mobspawn.getClassification()).add(mobspawn.getSpawnInfo());
		}
	}
}
