package no.nla.nlacritters.entity;

import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IAngerable;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import no.nla.nlacritters.client.NLASoundEvents;
import no.nla.nlacritters.util.ModConstants;

public class GorillaEntity extends AbstractMonkeyEntity implements IAngerable {

	protected static final DataParameter<Boolean> IS_SILVERBACK = EntityDataManager.createKey(GorillaEntity.class,
			DataSerializers.BOOLEAN);

	private int attackTimer;
	private int angerTime;
	private UUID angerTarget;

	public GorillaEntity(EntityType<? extends AbstractMonkeyEntity> type, World worldIn) {
		super(type, worldIn);
	}

	@Override
	protected void registerData() {
		super.registerData();
		this.dataManager.register(IS_SILVERBACK, Boolean.valueOf(false));
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld world, AgeableEntity parent) {
		// how is babby formed?
		return new GorillaEntity(ModEntityType.GORILLA, world);
	}

	protected SoundEvent getAmbientSound() {
		return NLASoundEvents.ENTITY_GORILLA_AMBIENT;
	}

	@Override
	public boolean isBreedingItem(ItemStack stack) {
		return stack.getItem() == Items.MELON_SLICE && !this.isSilverback();
	}

	@Nullable
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason,
			@Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
		int skin_index = worldIn.getRandom().nextInt(ModConstants.GORILLA_SKINS);
		this.dataManager.set(SKIN_INDEX, skin_index);
		this.dataManager.set(IS_SILVERBACK, skin_index == 4);
		if (skin_index == 3) {
			// hello there, EXPAND DONG
			if (worldIn.getRandom().nextInt(100) == 42)
				this.setCustomName(new StringTextComponent("Expand Dong"));
			else
				this.dataManager.set(SKIN_INDEX, 0);
		}
		return spawnDataIn;
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		this.targetSelector.addGoal(3,
				new NearestAttackableTargetGoal<>(this, PlayerEntity.class, 10, true, false, this::func_233680_b_));
	}

	public boolean isSilverback() {
		return this.dataManager.get(IS_SILVERBACK);
	}

	@Override
	public int getSkinIndex() {
		if (this.isChild() && this.isSilverback()) {
			return 0;
		} else {
			return this.dataManager.get(SKIN_INDEX);
		}
	}

	public static AttributeModifierMap.MutableAttribute prepareAttributes() {
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 22.0D)
				.createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.35D)
				.createMutableAttribute(Attributes.ATTACK_DAMAGE, 4.D);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn) {
		return this.isChild() ? 0.5F : 1.3F;
	}

	@Override
	public void livingTick() {
		super.livingTick();
		if (this.attackTimer > 0) {
			--this.attackTimer;
		}
	}

	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		this.writeAngerNBT(compound);
	}

	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		this.readAngerNBT((ServerWorld) this.world, compound);
	}

	@Override
	public boolean attackEntityAsMob(Entity entityIn) {
		this.attackTimer = 10;
		this.world.setEntityState(this, (byte) 4);
		float f = (float) (this.getAttributeValue(Attributes.ATTACK_DAMAGE) * (this.isSilverback() ? 1.3D : 0.8D));
		float f1 = (int) f > 0 ? f / 2.0F + (float) this.rand.nextInt((int) f) : f;
		boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), f1);
		if (flag && this.isSilverback()) {
			entityIn.setMotion(entityIn.getMotion().add(0.0D, (double) 0.4F, 0.0D));
			this.applyEnchantments(this, entityIn);
		}

		// this.playSound(SoundEvents.ENTITY_GORILLA_ATTACK, 1.0F, 1.0F);
		return flag;
	}

	public void setAngerTime(int time) {
		this.angerTime = time;
	}

	public int getAngerTime() {
		return this.angerTime;
	}

	public void setAngerTarget(@Nullable UUID target) {
		this.angerTarget = target;
	}

	public UUID getAngerTarget() {
		return this.angerTarget;
	}

	@Override
	public void func_230258_H__() {
	}
}
