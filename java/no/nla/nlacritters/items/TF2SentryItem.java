package no.nla.nlacritters.items;

import java.util.Objects;

import net.minecraft.block.BlockState;
import net.minecraft.block.FlowingFluidBlock;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public abstract class TF2SentryItem extends Item {
	
	public TF2SentryItem() {
		super(new Item.Properties().maxStackSize(1).group(ItemGroup.REDSTONE));
	}

	public ActionResultType onItemUse(ItemUseContext context) {
		World world = context.getWorld();
		if (!(world instanceof ServerWorld)) {
			return ActionResultType.SUCCESS;
		} else {
			ItemStack itemstack = context.getItem();
			BlockPos blockpos = context.getPos();
			Direction direction = context.getFace();
			BlockState blockstate = world.getBlockState(blockpos);

			BlockPos blockpos1;
			if (blockstate.getCollisionShape(world, blockpos).isEmpty()) {
				blockpos1 = blockpos;
			} else {
				blockpos1 = blockpos.offset(direction);
			}

			EntityType<? extends TF2SentryEntity> sentryType = this.getSentryType();
			TF2SentryEntity newSentry = sentryType.spawn((ServerWorld) world, null, null, context.getPlayer(), blockpos, SpawnReason.SPAWN_EGG, true, !Objects.equals(blockpos, blockpos1) && direction == Direction.UP);			
			if (newSentry != null) {
				itemstack.shrink(1);
				newSentry.setTeamColor(this.getTeamColor());
				newSentry.setYaw(context.getPlayer().rotationYaw);
				newSentry.startBuildTimer();
			}

			return ActionResultType.CONSUME;
		}
	}
	
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		RayTraceResult raytraceresult = rayTrace(worldIn, playerIn, RayTraceContext.FluidMode.SOURCE_ONLY);
		if (raytraceresult.getType() != RayTraceResult.Type.BLOCK) {
			return ActionResult.resultPass(itemstack);
		} else if (!(worldIn instanceof ServerWorld)) {
			return ActionResult.resultSuccess(itemstack);
		} else {
			BlockRayTraceResult blockraytraceresult = (BlockRayTraceResult) raytraceresult;
	         BlockPos blockpos = blockraytraceresult.getPos();
	         if (!(worldIn.getBlockState(blockpos).getBlock() instanceof FlowingFluidBlock)) {
	            return ActionResult.resultPass(itemstack);
	         } else if (worldIn.isBlockModifiable(playerIn, blockpos)
					&& playerIn.canPlayerEdit(blockpos, blockraytraceresult.getFace(), itemstack)) {
				EntityType<? extends TF2SentryEntity> sentryType = this.getSentryType();
				TF2SentryEntity newSentry = sentryType.spawn((ServerWorld) worldIn, null, null, playerIn, blockpos, SpawnReason.SPAWN_EGG, false,
						false);
				if (newSentry == null) {
					return ActionResult.resultPass(itemstack);
				} else {
					if (!playerIn.abilities.isCreativeMode) {
						itemstack.shrink(1);
					}
					
					newSentry.setTeamColor(this.getTeamColor());
					newSentry.setYaw(playerIn.rotationYaw);
					newSentry.startBuildTimer();
					playerIn.addStat(Stats.ITEM_USED.get(this));
					return ActionResult.resultConsume(itemstack);
				}
			} else {
				return ActionResult.resultFail(itemstack);
			}
		}
	}
	
	abstract int getTeamColor();
	abstract EntityType<? extends TF2SentryEntity> getSentryType();
}
