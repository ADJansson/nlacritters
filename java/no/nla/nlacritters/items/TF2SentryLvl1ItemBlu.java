package no.nla.nlacritters.items;

import net.minecraft.entity.EntityType;
import no.nla.nlacritters.entity.ModEntityType;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public class TF2SentryLvl1ItemBlu extends TF2SentryItem {

	public static final String NAME = "tf2sentry.lvl1.blu";
	
	public TF2SentryLvl1ItemBlu() {
		super();
	}

	@Override
	int getTeamColor() {
		return 1;
	}

	@Override
	EntityType<? extends TF2SentryEntity> getSentryType() {
		return ModEntityType.TF2_SENTRY_LVL1;
	}
}
