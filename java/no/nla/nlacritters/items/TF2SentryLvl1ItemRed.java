package no.nla.nlacritters.items;

import net.minecraft.entity.EntityType;
import no.nla.nlacritters.entity.ModEntityType;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public class TF2SentryLvl1ItemRed extends TF2SentryItem {

	public static final String NAME = "tf2sentry.lvl1.red";
	
	public TF2SentryLvl1ItemRed() {
		super();
	}

	@Override
	int getTeamColor() {
		return 0;
	}
	
	@Override
	EntityType<? extends TF2SentryEntity> getSentryType() {
		return ModEntityType.TF2_SENTRY_LVL1;
	}
}
