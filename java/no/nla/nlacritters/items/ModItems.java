package no.nla.nlacritters.items;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;
import no.nla.nlacritters.client.Main;

public class ModItems {

	public static final Item TF2_SENTRY_LVL1_RED = new TF2SentryLvl1ItemRed().setRegistryName(Main.MODID, TF2SentryLvl1ItemRed.NAME);
	public static final Item TF2_SENTRY_LVL1_BLU = new TF2SentryLvl1ItemBlu().setRegistryName(Main.MODID, TF2SentryLvl1ItemBlu.NAME);
	public static final Item TF2_SENTRY_LVL2_RED = new TF2SentryLvl2ItemRed().setRegistryName(Main.MODID, TF2SentryLvl2ItemRed.NAME);
	public static final Item TF2_SENTRY_LVL2_BLU = new TF2SentryLvl2ItemBlu().setRegistryName(Main.MODID, TF2SentryLvl2ItemBlu.NAME);
	
	public static void registerItems(final RegistryEvent.Register<Item> event) {

		final IForgeRegistry<Item> registry = event.getRegistry();
		registry.register(TF2_SENTRY_LVL1_RED);
		registry.register(TF2_SENTRY_LVL1_BLU);
		registry.register(TF2_SENTRY_LVL2_RED);
		registry.register(TF2_SENTRY_LVL2_BLU);
	}
}
