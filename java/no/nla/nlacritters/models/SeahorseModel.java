package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import no.nla.nlacritters.entity.SeahorseEntity;

public class SeahorseModel extends EntityModel<SeahorseEntity> {

	private final ModelRenderer body;
	private final ModelRenderer tail0;
	private final ModelRenderer tail1;
	private final ModelRenderer tail2;
	private final ModelRenderer tail3;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer tailfin;
	private final ModelRenderer neck;
	private final ModelRenderer cube_r3;
	private final ModelRenderer head;
	private final ModelRenderer muzzle;
	private final ModelRenderer bridle1;
	private final ModelRenderer bridle0;
	private final ModelRenderer rgtLeg0;
	private final ModelRenderer rgtLeg1;
	private final ModelRenderer rgtHoof;
	private final ModelRenderer lftLeg0;
	private final ModelRenderer lftLeg1;
	private final ModelRenderer lftHoof;
	private final ModelRenderer saddle;

	public SeahorseModel() {
		textureWidth = 128;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 7.0F, -0.5F);
		body.setTextureOffset(70, 35).addBox(-5.0F, -5.0F, -9.5F, 10.0F, 10.0F, 19.0F, 0.0F, false);

		tail0 = new ModelRenderer(this);
		tail0.setRotationPoint(0.0F, 0.0F, 9.5F);
		body.addChild(tail0);
		tail0.setTextureOffset(0, 46).addBox(-4.5F, -4.5F, -0.5F, 9.0F, 9.0F, 9.0F, 0.0F, false);

		tail1 = new ModelRenderer(this);
		tail1.setRotationPoint(0.0F, 0.0F, 8.5F);
		tail0.addChild(tail1);
		tail1.setTextureOffset(37, 48).addBox(-3.5F, -3.5F, 0.0F, 7.0F, 7.0F, 9.0F, 0.0F, false);

		tail2 = new ModelRenderer(this);
		tail2.setRotationPoint(0.0F, 0.0F, 9.0F);
		tail1.addChild(tail2);
		tail2.setTextureOffset(96, 0).addBox(-3.0F, -2.0F, 0.0F, 6.0F, 4.0F, 10.0F, 0.0F, false);

		tail3 = new ModelRenderer(this);
		tail3.setRotationPoint(0.0F, 0.0F, 10.0F);
		tail2.addChild(tail3);
		tail3.setTextureOffset(77, 9).addBox(-2.5F, -1.0F, 0.0F, 5.0F, 2.0F, 8.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-5.5F, 3.0F, -7.5F);
		tail3.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.5236F, 0.0F, -2.0944F);
		cube_r1.setTextureOffset(67, 36).addBox(-0.5F, -4.0F, -3.5F, 1.0F, 7.0F, 9.0F, 0.0F, true);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(5.5F, 3.0F, -7.5F);
		tail3.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.5236F, 0.0F, 2.0944F);
		cube_r2.setTextureOffset(67, 36).addBox(-0.5F, -4.0F, -3.5F, 1.0F, 7.0F, 9.0F, 0.0F, false);

		tailfin = new ModelRenderer(this);
		tailfin.setRotationPoint(0.0F, 0.0F, 8.0F);
		tail3.addChild(tailfin);
		tailfin.setTextureOffset(68, 19).addBox(-7.5F, -1.0F, 0.0F, 15.0F, 1.0F, 15.0F, 0.0F, false);

		neck = new ModelRenderer(this);
		neck.setRotationPoint(0.0F, 1.0F, -9.5F);
		body.addChild(neck);
		neck.setTextureOffset(43, 26).addBox(-2.0F, -12.0F, 0.0F, 4.0F, 12.0F, 7.0F, 0.0F, false);
		neck.setTextureOffset(19, 16).addBox(0.5F, -19.0F, 6.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		neck.setTextureOffset(19, 16).addBox(-2.5F, -19.0F, 6.0F, 2.0F, 3.0F, 1.0F, 0.0F, true);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.5F, -15.0F, 8.5F);
		neck.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.3491F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(23, 20).addBox(-0.5F, -8.0F, -4.5F, 1.0F, 16.0F, 9.0F, 0.0F, false);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 0.75F, 6.75F);
		neck.addChild(head);
		head.setTextureOffset(0, 13).addBox(-2.5F, -17.75F, -6.75F, 5.0F, 5.0F, 7.0F, 0.0F, false);

		muzzle = new ModelRenderer(this);
		muzzle.setRotationPoint(0.0F, -15.25F, -3.25F);
		head.addChild(muzzle);
		muzzle.setTextureOffset(0, 25).addBox(-2.0F, -1.5F, -8.5F, 4.0F, 4.0F, 5.0F, 0.0F, false);

		bridle1 = new ModelRenderer(this);
		bridle1.setRotationPoint(0.0F, 0.5F, -5.0F);
		muzzle.addChild(bridle1);
		bridle1.setTextureOffset(19, 0).addBox(-2.0F, -2.0F, -0.5F, 4.0F, 4.0F, 2.0F, 0.1F, false);
		bridle1.setTextureOffset(29, 5).addBox(1.5F, -0.5F, -2.5F, 1.0F, 2.0F, 2.0F, 0.1F, false);
		bridle1.setTextureOffset(29, 5).addBox(-2.5F, -0.5F, -2.5F, 1.0F, 2.0F, 2.0F, 0.1F, true);

		bridle0 = new ModelRenderer(this);
		bridle0.setRotationPoint(0.0F, -15.25F, -3.25F);
		head.addChild(bridle0);
		bridle0.setTextureOffset(0, 0).addBox(-2.5F, -2.5F, -3.5F, 5.0F, 5.0F, 7.0F, 0.1F, false);

		rgtLeg0 = new ModelRenderer(this);
		rgtLeg0.setRotationPoint(-2.5F, 4.0F, -7.0F);
		body.addChild(rgtLeg0);
		rgtLeg0.setTextureOffset(67, 0).addBox(-3.0F, -2.0F, -1.0F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		rgtLeg1 = new ModelRenderer(this);
		rgtLeg1.setRotationPoint(-2.0F, 6.0F, 1.0F);
		rgtLeg0.addChild(rgtLeg1);
		rgtLeg1.setTextureOffset(67, 17).addBox(-1.0F, 0.0F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, false);
		rgtLeg1.setTextureOffset(68, 39).addBox(-0.5F, -1.0F, -1.0F, 1.0F, 6.0F, 7.0F, 0.0F, true);

		rgtHoof = new ModelRenderer(this);
		rgtHoof.setRotationPoint(0.0F, 5.0F, 0.0F);
		rgtLeg1.addChild(rgtHoof);
		rgtHoof.setTextureOffset(67, 27).addBox(-1.5F, 0.0F, -2.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);

		lftLeg0 = new ModelRenderer(this);
		lftLeg0.setRotationPoint(2.5F, 4.0F, -7.0F);
		body.addChild(lftLeg0);
		lftLeg0.setTextureOffset(67, 0).addBox(0.0F, -2.0F, -1.0F, 3.0F, 8.0F, 4.0F, 0.0F, true);

		lftLeg1 = new ModelRenderer(this);
		lftLeg1.setRotationPoint(2.0F, 6.0F, 1.0F);
		lftLeg0.addChild(lftLeg1);
		lftLeg1.setTextureOffset(67, 17).addBox(-2.0F, 0.0F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, true);
		lftLeg1.setTextureOffset(68, 39).addBox(-0.5F, -1.0F, -1.0F, 1.0F, 6.0F, 7.0F, 0.0F, false);

		lftHoof = new ModelRenderer(this);
		lftHoof.setRotationPoint(0.0F, 5.0F, 0.0F);
		lftLeg1.addChild(lftHoof);
		lftHoof.setTextureOffset(67, 27).addBox(-2.5F, 0.0F, -2.0F, 4.0F, 3.0F, 4.0F, 0.0F, true);

		saddle = new ModelRenderer(this);
		saddle.setRotationPoint(0.0F, -1.0F, 4.5F);
		body.addChild(saddle);
		saddle.setTextureOffset(26, 0).addBox(-5.0F, -4.0F, -4.5F, 10.0F, 9.0F, 9.0F, 0.1F, false);
	}

	@Override
	public void setRotationAngles(SeahorseEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {

		this.body.rotateAngleX = headPitch * ((float) Math.PI / 180F);
		this.body.rotateAngleY = netHeadYaw * ((float) Math.PI / 180F);
		if (Entity.horizontalMag(entityIn.getMotion()) > 1.0E-7D) {
			this.body.rotateAngleX += -0.05F + -0.05F * MathHelper.cos(ageInTicks * 0.3F);
			this.tail0.rotateAngleX = -0.1F * MathHelper.cos(ageInTicks * 0.3F);
			this.tail1.rotateAngleX = -0.1F * MathHelper.cos(ageInTicks * 0.3F);
			this.tail2.rotateAngleX = -0.1F * MathHelper.cos(ageInTicks * 0.3F);
			this.tail3.rotateAngleX = -0.1F * MathHelper.cos(ageInTicks * 0.3F);
			this.tailfin.rotateAngleX = -0.2F * MathHelper.sin(ageInTicks * 0.3F);
		}

		RotationUtil.rotateDegrees(this.neck, 30.F, 0.F, 0.F);
		if (entityIn.isInWater()) {
			float fxR0 = (float) ((MathHelper.cos(limbSwing * 0.0662F + (float) Math.PI) * 1.95F * limbSwingAmount) * 180.F / Math.PI);
			float fxR1 = (float) ((Math.max(0, MathHelper.cos(limbSwing * 0.0662F + (float) Math.PI) * 0.75F * limbSwingAmount)) * 180.F / Math.PI);
			float fxL0 = (float) ((MathHelper.cos(limbSwing * 0.0662F) * 1.95F * limbSwingAmount) * 180.F / Math.PI);
			float fxL1 = (float) ((Math.max(0, MathHelper.cos(limbSwing * 0.0662F) * 0.75F * limbSwingAmount) * 180.F / Math.PI));
			
			
			RotationUtil.rotateDegrees(this.lftLeg0, fxR0 * 0.1F + 20.F, 0.F, 0.F);
			RotationUtil.rotateDegrees(this.rgtLeg0, fxL0 * 0.1F + 20.F, 0.F, 0.F);
			RotationUtil.rotateDegrees(this.lftLeg1, fxR1 * 0.1F + 50.F, 0.F, 0.F);
			RotationUtil.rotateDegrees(this.rgtLeg1, fxL1 * 0.1F + 50.F, 0.F, 0.F);
			
			//this.rgtLeg0.rotateAngleX = fxR0;
			//this.lftLeg0.rotateAngleX = fxL0;
			//this.rgtLeg1.rotateAngleX = fxR1;
			//this.lftLeg1.rotateAngleX = fxL1;
		} else {
			RotationUtil.rotateDegrees(this.lftLeg0, -90.F, 0.F, 0.F);
			RotationUtil.rotateDegrees(this.rgtLeg0, -90.F, 0.F, 0.F);
		}
	}

	public void setLivingAnimations(SeahorseEntity entityIn, float limbSwing, float limbSwingAmount,
			float partialTick) {
		super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);

		this.saddle.showModel = entityIn.isSaddled();
		this.bridle0.showModel = entityIn.isSaddled();
		this.bridle1.showModel = entityIn.isSaddled();
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		this.body.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
