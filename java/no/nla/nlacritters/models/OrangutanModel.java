package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import no.nla.nlacritters.entity.OrangutanEntity;

public class OrangutanModel extends EntityModel<OrangutanEntity> {

	private final ModelRenderer body;
	private final ModelRenderer rgtArm;
	private final ModelRenderer arm_r1;
	private final ModelRenderer lftArm;
	private final ModelRenderer arm_r2;
	private final ModelRenderer rgtLeg;
	private final ModelRenderer rgtLeg_r1;
	private final ModelRenderer lftLeg;
	private final ModelRenderer lftLeg_r1;
	private final ModelRenderer head;
	private final ModelRenderer jaw_r1;
	private final ModelRenderer head_r1;

	public OrangutanModel() {
		textureWidth = 64;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 17.0F, 4.0F);
		setRotationAngle(body, 0.9599F, 0.0F, 0.0F);
		body.setTextureOffset(0, 14).addBox(-4.0F, -10.0F, -3.0F, 8.0F, 10.0F, 6.0F, 0.0F, false);
		body.setTextureOffset(0, 39).addBox(-3.5F, -0.0924F, -1.9434F, 7.0F, 3.0F, 5.0F, 0.0F, false);

		rgtArm = new ModelRenderer(this);
		rgtArm.setRotationPoint(-4.0F, -8.0F, 0.0F);
		body.addChild(rgtArm);

		arm_r1 = new ModelRenderer(this);
		arm_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rgtArm.addChild(arm_r1);
		setRotationAngle(arm_r1, -0.9599F, 0.0F, 0.0F);
		arm_r1.setTextureOffset(30, 2).addBox(-3.0F, -1.25F, -2.0F, 3.0F, 13.0F, 3.0F, 0.0F, false);

		lftArm = new ModelRenderer(this);
		lftArm.setRotationPoint(4.0F, -8.1434F, 0.2048F);
		body.addChild(lftArm);

		arm_r2 = new ModelRenderer(this);
		arm_r2.setRotationPoint(0.0F, 0.0F, 0.0F);
		lftArm.addChild(arm_r2);
		setRotationAngle(arm_r2, -0.9599F, 0.0F, 0.0F);
		arm_r2.setTextureOffset(30, 2).addBox(0.0F, -1.0F, -2.0F, 3.0F, 13.0F, 3.0F, 0.0F, true);

		rgtLeg = new ModelRenderer(this);
		rgtLeg.setRotationPoint(-5.0F, 2.2465F, -0.8177F);
		body.addChild(rgtLeg);
		setRotationAngle(rgtLeg, -0.6981F, 0.0F, 0.0F);

		rgtLeg_r1 = new ModelRenderer(this);
		rgtLeg_r1.setRotationPoint(1.5F, -0.2465F, 0.8177F);
		rgtLeg.addChild(rgtLeg_r1);
		setRotationAngle(rgtLeg_r1, -0.2618F, 0.0F, 0.0F);
		rgtLeg_r1.setTextureOffset(42, 8).addBox(-1.5F, -1.2535F, -1.8177F, 3.0F, 7.0F, 3.0F, 0.0F, false);

		lftLeg = new ModelRenderer(this);
		lftLeg.setRotationPoint(5.0F, 1.6729F, 0.0015F);
		body.addChild(lftLeg);
		setRotationAngle(lftLeg, -0.6981F, 0.0F, 0.0F);

		lftLeg_r1 = new ModelRenderer(this);
		lftLeg_r1.setRotationPoint(-1.5F, -0.2465F, 0.8177F);
		lftLeg.addChild(lftLeg_r1);
		setRotationAngle(lftLeg_r1, -0.2618F, 0.0F, 0.0F);
		lftLeg_r1.setTextureOffset(42, 8).addBox(-1.5F, -0.2535F, -1.8177F, 3.0F, 7.0F, 3.0F, 0.0F, true);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, -10.0F, -1.0F);
		body.addChild(head);

		jaw_r1 = new ModelRenderer(this);
		jaw_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.addChild(jaw_r1);
		setRotationAngle(jaw_r1, -0.9163F, 0.0F, 0.0F);
		jaw_r1.setTextureOffset(18, 30).addBox(-2.0F, -0.8797F, -4.3645F, 4.0F, 4.0F, 3.0F, 0.0F, false);

		head_r1 = new ModelRenderer(this);
		head_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.addChild(head_r1);
		setRotationAngle(head_r1, -0.6981F, 0.0F, 0.0F);
		head_r1.setTextureOffset(0, 2).addBox(-3.0F, -5.0F, -3.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(OrangutanEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		this.head.rotateAngleX = headPitch * ((float) Math.PI / 180F);
		this.head.rotateAngleY = netHeadYaw * ((float) Math.PI / 180F) * 0.5F;

		if (entityIn.isOnLadder()) {
			this.body.rotateAngleX = 0.F;
			RotationUtil.rotateDegrees(this.rgtArm, -70.F, 0.F, 0.F);
			RotationUtil.rotateDegrees(this.lftArm, -70.F, 0.F, 0.F);
		} else {
			this.body.rotateAngleX = ((float) Math.PI / 180F * 55);
			this.rgtArm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 0.75F * limbSwingAmount;
			this.lftArm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount;
		}

		this.rgtLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount
				- ((float) Math.PI / 180F * 55);
		this.lftLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 0.75F * limbSwingAmount
				- ((float) Math.PI / 180F * 55);
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		this.body.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}

	public void setRotationAngle(ModelRenderer ModelRenderer, float x, float y, float z) {
		ModelRenderer.rotateAngleX = x;
		ModelRenderer.rotateAngleY = y;
		ModelRenderer.rotateAngleZ = z;
	}
}
