package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryLvl2Entity;

public class TF2SentryLvl2Model extends EntityModel<TF2SentryLvl2Entity> {
	private final ModelRenderer base;
	private final ModelRenderer tripod;
	private final ModelRenderer cube_r1;
	private final ModelRenderer turret;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer barrel_r;
	private final ModelRenderer cube_r4;
	private final ModelRenderer barrel_l;
	private final ModelRenderer cube_r5;
	private final ModelRenderer toolbox;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer legs;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	
	public TF2SentryLvl2Model() {
		textureWidth = 64;
		textureHeight = 32;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 16.0F, 0.0F);
		
		tripod = new ModelRenderer(this);
		tripod.setRotationPoint(0.0F, -2.0F, 0.0F);
		base.addChild(tripod);
		
		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		tripod.addChild(cube_r1);
		setRotationAngle(cube_r1, -0.3491F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(33, 0).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);

		turret = new ModelRenderer(this);
		turret.setRotationPoint(0.0F, -1.0F, 0.0F);
		tripod.addChild(turret);
		turret.setTextureOffset(9, 17).addBox(-5.5F, 0.0F, -1.0F, 11.0F, 1.0F, 2.0F, 0.0F, false);
		turret.setTextureOffset(0, 0).addBox(-4.0F, -4.0F, 3.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		turret.setTextureOffset(2, 14).addBox(-3.0F, -4.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
		turret.setTextureOffset(39, 6).addBox(5.0F, -1.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		turret.setTextureOffset(39, 6).addBox(-6.0F, -1.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.0F, 1.0F, 0.0F);
		turret.addChild(cube_r2);
		setRotationAngle(cube_r2, -0.3491F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(33, 0).addBox(-0.5F, -5.25F, 6.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.0F, 1.0F, 0.0F);
		turret.addChild(cube_r3);
		setRotationAngle(cube_r3, 1.1781F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(33, 0).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);

		barrel_r = new ModelRenderer(this);
		barrel_r.setRotationPoint(-5.5F, -2.5F, 0.0F);
		turret.addChild(barrel_r);
		barrel_r.setTextureOffset(20, 12).addBox(-1.5F, -1.5F, -2.0F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(1.0F, 0.0F, -8.0F);
		barrel_r.addChild(cube_r4);
		setRotationAngle(cube_r4, -1.5708F, 0.0F, 0.0F);
		cube_r4.setTextureOffset(39, 0).addBox(-0.5F, -6.0F, 0.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);
		cube_r4.setTextureOffset(39, 0).addBox(-2.5F, -6.0F, 0.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);
		cube_r4.setTextureOffset(39, 0).addBox(-2.5F, -6.0F, -1.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);
		cube_r4.setTextureOffset(39, 0).addBox(-0.5F, -6.0F, -1.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);

		barrel_l = new ModelRenderer(this);
		barrel_l.setRotationPoint(5.5F, -2.5F, 0.0F);
		turret.addChild(barrel_l);
		barrel_l.setTextureOffset(20, 12).addBox(-1.5F, -1.5F, -2.0F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(1.0F, 0.0F, -8.0F);
		barrel_l.addChild(cube_r5);
		setRotationAngle(cube_r5, -1.5708F, 0.0F, 0.0F);
		cube_r5.setTextureOffset(39, 0).addBox(-0.5F, -6.0F, 0.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);
		cube_r5.setTextureOffset(39, 0).addBox(-2.5F, -6.0F, 0.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);
		cube_r5.setTextureOffset(39, 0).addBox(-2.5F, -6.0F, -1.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);
		cube_r5.setTextureOffset(39, 0).addBox(-0.5F, -6.0F, -1.5F, 1.0F, 12.0F, 1.0F, 0.0F, false);

		toolbox = new ModelRenderer(this);
		toolbox.setRotationPoint(-0.5F, 5.5F, 0.0F);
		base.addChild(toolbox);		

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(0.5F, 0.5F, -0.5F);
		toolbox.addChild(cube_r6);
		setRotationAngle(cube_r6, 1.5708F, -0.6545F, -1.5708F);
		cube_r6.setTextureOffset(32, 17).addBox(2.5F, 5.0F, -5.5F, 5.0F, 2.0F, 11.0F, 1.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(0.5F, -4.5F, 0.0F);
		toolbox.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.0F, -1.5708F, 0.0F);
		cube_r7.setTextureOffset(32, 2).addBox(-3.0F, 4.8F, -5.5F, 5.0F, 1.0F, 11.0F, 1.0F, false);

		legs = new ModelRenderer(this);
		legs.setRotationPoint(0.0F, -0.5F, -1.0F);
		base.addChild(legs);
		setRotationAngle(legs, -0.0436F, 0.0F, 0.0F);
		
		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(0.0F, 0.0F, 0.0F);
		legs.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.48F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(0, 0).addBox(1.0F, 8.0F, 3.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 0).addBox(-2.0F, 8.0F, 3.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(2.0F, 2.5F, -0.5F);
		legs.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.7675F, 0.6484F, 1.0115F);
		cube_r9.setTextureOffset(30, 30).addBox(-1.0F, -0.5F, -0.5F, 10.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(-2.0F, 2.5F, -0.5F);
		legs.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.7675F, -0.6484F, -1.0115F);
		cube_r10.setTextureOffset(30, 30).addBox(-9.0F, -0.5F, -0.5F, 10.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(0.0F, 3.0F, 0.0F);
		legs.addChild(cube_r11);
		setRotationAngle(cube_r11, -0.3491F, 0.0F, 0.0F);
		cube_r11.setTextureOffset(0, 20).addBox(-2.0F, -1.0F, -2.0F, 4.0F, 2.0F, 10.0F, 0.0F, false);
	}
	
	@Override
	public void setRotationAngles(TF2SentryLvl2Entity entityIn, float limbSwing, float limbSwingAmount,
			float ageInTicks, float netHeadYaw, float headPitch) {
		
		if (entityIn.isReady()) {
			this.tripod.rotationPointY = 0.F;
			this.turret.rotateAngleX = headPitch * ((float) Math.PI / 180F);
			this.tripod.rotateAngleY = netHeadYaw * ((float) Math.PI / 180F);
			this.legs.showModel = true;
			this.toolbox.showModel = false;
			this.barrel_r.showModel = true;
			this.barrel_l.showModel = true;
			if (entityIn.getAttackTarget() != null) {
				this.barrel_l.rotateAngleZ -= 0.12F;
				this.barrel_r.rotateAngleZ += 0.12F;
			} else {
				this.barrel_l.rotateAngleZ = 0.F;
				this.barrel_r.rotateAngleZ = 0.F;
			}
		} else {
			// play building animation
			this.tripod.rotationPointY = entityIn.tripodBuildOffsetY;
			this.tripod.rotateAngleY = entityIn.tripodBuildRotation;
			this.turret.rotateAngleX = entityIn.turretBuildRotation;
			this.legs.showModel = false;
			this.toolbox.showModel = true;
			this.barrel_r.showModel = false;
			this.barrel_l.showModel = false;
		}	
	}
	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		this.base.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);	
	}
	
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
