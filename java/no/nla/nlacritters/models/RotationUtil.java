package no.nla.nlacritters.models;

import net.minecraft.client.renderer.model.ModelRenderer;

public class RotationUtil {
	public static void rotateDegrees(ModelRenderer model, float xRot, float yRot, float zRot) {
		model.rotateAngleX = (float) (xRot * Math.PI / 180.F);
		model.rotateAngleY = (float) (yRot * Math.PI / 180.F);
		model.rotateAngleZ = (float) (zRot * Math.PI / 180.F);
	}
}
