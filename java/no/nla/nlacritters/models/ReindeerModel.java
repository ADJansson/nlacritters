package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import no.nla.nlacritters.entity.ReindeerEntity;

public class ReindeerModel extends EntityModel<ReindeerEntity> {

	private float headRotationAngleX;

	private final ModelRenderer body;
	private final ModelRenderer tail_r1;
	private final ModelRenderer lftFrontLeg;
	private final ModelRenderer lftHindLeg;
	private final ModelRenderer rgtFrontLeg;
	private final ModelRenderer rgtRearLeg;
	private final ModelRenderer neck_r1;
	private final ModelRenderer head;
	private final ModelRenderer rgtEar_r1;
	private final ModelRenderer lftEar_r1;
	private final ModelRenderer lftAntler0;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r7_r1;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r8_r1;
	private final ModelRenderer lftAntler1;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r9_r1;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r10_r1;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r11_r1;
	private final ModelRenderer cube_r12;
	private final ModelRenderer cube_r12_r1;
	private final ModelRenderer rgtAntler0;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r1_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r2_r1;
	private final ModelRenderer rgtAntler1;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r3_r1;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r4_r1;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r5_r1;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r6_r1;

	public ReindeerModel() {
		textureWidth = 64;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 11.0F, 0.0F);
		body.setTextureOffset(0, 0).addBox(-5.5F, -10.0F, -9.0F, 11.0F, 10.0F, 21.0F, 0.0F, false);

		tail_r1 = new ModelRenderer(this);
		tail_r1.setRotationPoint(1.5F, -5.75F, 11.0F);
		body.addChild(tail_r1);
		setRotationAngle(tail_r1, 0.1309F, 0.0F, 0.0F);
		tail_r1.setTextureOffset(43, 14).addBox(-3.0F, -4.0F, 0.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);

		lftFrontLeg = new ModelRenderer(this);
		lftFrontLeg.setRotationPoint(5.5F, 0.0F, -7.0F);
		body.addChild(lftFrontLeg);
		lftFrontLeg.setTextureOffset(1, 1).addBox(-3.0F, 0.0F, -1.5F, 3.0F, 13.0F, 3.0F, 0.0F, false);

		lftHindLeg = new ModelRenderer(this);
		lftHindLeg.setRotationPoint(5.5F, 0.0F, 9.5F);
		body.addChild(lftHindLeg);
		lftHindLeg.setTextureOffset(1, 1).addBox(-3.0F, 0.0F, -1.5F, 3.0F, 13.0F, 3.0F, 0.0F, false);

		rgtFrontLeg = new ModelRenderer(this);
		rgtFrontLeg.setRotationPoint(-5.5F, 0.0F, -7.0F);
		body.addChild(rgtFrontLeg);
		rgtFrontLeg.setTextureOffset(1, 1).addBox(0.0F, 0.0F, -1.5F, 3.0F, 13.0F, 3.0F, 0.0F, true);

		rgtRearLeg = new ModelRenderer(this);
		rgtRearLeg.setRotationPoint(-5.5F, 0.0F, 9.5F);
		body.addChild(rgtRearLeg);
		rgtRearLeg.setTextureOffset(1, 1).addBox(0.0F, 0.0F, -1.5F, 3.0F, 13.0F, 3.0F, 0.0F, true);

		neck_r1 = new ModelRenderer(this);
		neck_r1.setRotationPoint(0.0F, -6.6F, -5.5F);
		body.addChild(neck_r1);
		setRotationAngle(neck_r1, -0.8727F, 0.0F, 0.0F);
		neck_r1.setTextureOffset(25, 32).addBox(-2.5F, -4.5F, -10.5F, 5.0F, 9.0F, 11.0F, 0.0F, false);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 4.0F, -10.0F);
		neck_r1.addChild(head);
		head.setTextureOffset(0, 46).addBox(-2.5F, -8.4037F, -5.2905F, 5.0F, 13.0F, 5.0F, 0.0F, false);

		rgtEar_r1 = new ModelRenderer(this);
		rgtEar_r1.setRotationPoint(-4.0F, -6.9F, -3.0F);
		head.addChild(rgtEar_r1);
		setRotationAngle(rgtEar_r1, 1.5708F, -0.4363F, 0.384F);
		rgtEar_r1.setTextureOffset(50, 4).addBox(-2.0F, -1.5F, -0.5F, 4.0F, 3.0F, 1.0F, 0.0F, false);

		lftEar_r1 = new ModelRenderer(this);
		lftEar_r1.setRotationPoint(4.0F, -6.9F, -3.0F);
		head.addChild(lftEar_r1);
		setRotationAngle(lftEar_r1, 1.5708F, 0.4363F, -0.384F);
		lftEar_r1.setTextureOffset(50, 4).addBox(-2.0F, -1.5F, -0.5F, 4.0F, 3.0F, 1.0F, 0.0F, true);

		lftAntler0 = new ModelRenderer(this);
		lftAntler0.setRotationPoint(0.0F, 14.6F, 15.5F);
		head.addChild(lftAntler0);
		setRotationAngle(lftAntler0, 1.5708F, -0.0873F, 0.0F);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(3.4919F, -24.4124F, 0.4946F);
		lftAntler0.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.5236F, -1.1345F, 0.0F);

		cube_r7_r1 = new ModelRenderer(this);
		cube_r7_r1.setRotationPoint(-17.3657F, 16.2409F, -12.4327F);
		cube_r7.addChild(cube_r7_r1);
		setRotationAngle(cube_r7_r1, 0.4956F, 0.9629F, 2.1746F);
		cube_r7_r1.setTextureOffset(7, 39).addBox(-33.6673F, -25.8861F, -1.4732F, 1.0F, 1.0F, 4.0F, 0.0F, true);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(3.0F, -22.7263F, -1.3881F);
		lftAntler0.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.733F, 0.0F, -0.3316F);

		cube_r8_r1 = new ModelRenderer(this);
		cube_r8_r1.setRotationPoint(-10.2594F, 1.9709F, -21.9241F);
		cube_r8.addChild(cube_r8_r1);
		setRotationAngle(cube_r8_r1, -1.9697F, 0.2429F, 0.5572F);
		cube_r8_r1.setTextureOffset(5, 37).addBox(2.6534F, -40.9063F, -13.4773F, 1.0F, 1.0F, 6.0F, 0.0F, true);

		lftAntler1 = new ModelRenderer(this);
		lftAntler1.setRotationPoint(3.0F, -23.7263F, 0.6119F);
		lftAntler0.addChild(lftAntler1);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(1.8F, -11.5F, -7.0F);
		lftAntler1.addChild(cube_r9);
		setRotationAngle(cube_r9, -0.6545F, -0.2618F, 0.0F);

		cube_r9_r1 = new ModelRenderer(this);
		cube_r9_r1.setRotationPoint(-8.6167F, 29.5503F, 12.5688F);
		cube_r9.addChild(cube_r9_r1);
		setRotationAngle(cube_r9_r1, -1.9832F, 0.1914F, 0.5733F);
		cube_r9_r1.setTextureOffset(5, 37).addBox(-12.9495F, -3.2995F, -55.0038F, 1.0F, 1.0F, 6.0F, 0.0F, true);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(3.1F, -9.9613F, -5.8491F);
		lftAntler1.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.3054F, -0.6981F, 0.0F);

		cube_r10_r1 = new ModelRenderer(this);
		cube_r10_r1.setRotationPoint(-12.9333F, 27.085F, -12.3884F);
		cube_r10.addChild(cube_r10_r1);
		setRotationAngle(cube_r10_r1, -1.1389F, 1.1095F, 0.9732F);
		cube_r10_r1.setTextureOffset(6, 38).addBox(-27.4253F, -38.5657F, -22.3353F, 1.0F, 1.0F, 5.0F, 0.0F, true);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(2.4443F, -5.4751F, -3.533F);
		lftAntler1.addChild(cube_r11);
		setRotationAngle(cube_r11, -1.85F, 0.0F, -0.733F);

		cube_r11_r1 = new ModelRenderer(this);
		cube_r11_r1.setRotationPoint(-21.8968F, 9.0573F, 17.1814F);
		cube_r11.addChild(cube_r11_r1);
		setRotationAngle(cube_r11_r1, -1.3401F, -0.7088F, -1.1488F);
		cube_r11_r1.setTextureOffset(7, 39).addBox(17.5234F, 42.9255F, -12.4992F, 1.0F, 1.0F, 4.0F, 0.0F, true);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(0.0F, 0.0F, 0.0F);
		lftAntler1.addChild(cube_r12);
		setRotationAngle(cube_r12, 1.9548F, 0.0F, 0.192F);

		cube_r12_r1 = new ModelRenderer(this);
		cube_r12_r1.setRotationPoint(-0.627F, -23.2626F, -12.262F);
		cube_r12.addChild(cube_r12_r1);
		setRotationAngle(cube_r12_r1, -2.0466F, -0.2901F, -0.2024F);
		cube_r12_r1.setTextureOffset(0, 32).addBox(-9.0402F, -25.211F, 32.7506F, 1.0F, 1.0F, 11.0F, 0.0F, true);

		rgtAntler0 = new ModelRenderer(this);
		rgtAntler0.setRotationPoint(0.0F, 15.6F, 15.5F);
		head.addChild(rgtAntler0);
		setRotationAngle(rgtAntler0, 1.5708F, 0.0873F, 0.0F);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-3.4919F, -24.6124F, 0.4946F);
		rgtAntler0.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.5236F, 1.1345F, 0.0F);

		cube_r1_r1 = new ModelRenderer(this);
		cube_r1_r1.setRotationPoint(16.6021F, 17.1524F, -12.5199F);
		cube_r1.addChild(cube_r1_r1);
		setRotationAngle(cube_r1_r1, 0.487F, -0.9602F, -2.1711F);
		cube_r1_r1.setTextureOffset(7, 39).addBox(33.1408F, -25.9408F, -1.6878F, 1.0F, 1.0F, 4.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-3.0F, -22.9263F, -1.3881F);
		rgtAntler0.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.733F, 0.0F, 0.3316F);

		cube_r2_r1 = new ModelRenderer(this);
		cube_r2_r1.setRotationPoint(10.5514F, 3.1215F, -21.8127F);
		cube_r2.addChild(cube_r2_r1);
		setRotationAngle(cube_r2_r1, -1.9677F, -0.2427F, -0.5639F);
		cube_r2_r1.setTextureOffset(5, 37).addBox(-3.6274F, -41.2134F, -13.8996F, 1.0F, 1.0F, 6.0F, 0.0F, false);

		rgtAntler1 = new ModelRenderer(this);
		rgtAntler1.setRotationPoint(-3.0F, -23.9263F, 0.6119F);
		rgtAntler0.addChild(rgtAntler1);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-1.8F, -11.5F, -7.0F);
		rgtAntler1.addChild(cube_r3);
		setRotationAngle(cube_r3, -0.6545F, 0.2618F, 0.0F);

		cube_r3_r1 = new ModelRenderer(this);
		cube_r3_r1.setRotationPoint(8.4171F, 29.7064F, 13.7339F);
		cube_r3.addChild(cube_r3_r1);
		setRotationAngle(cube_r3_r1, -1.985F, -0.1853F, -0.5746F);
		cube_r3_r1.setTextureOffset(5, 37).addBox(12.0906F, -2.9477F, -55.3639F, 1.0F, 1.0F, 6.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(-3.1F, -9.9613F, -5.8491F);
		rgtAntler1.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.3054F, 0.6981F, 0.0F);

		cube_r4_r1 = new ModelRenderer(this);
		cube_r4_r1.setRotationPoint(12.402F, 28.0802F, -12.0024F);
		cube_r4.addChild(cube_r4_r1);
		setRotationAngle(cube_r4_r1, -1.1389F, -1.1049F, -0.9779F);
		cube_r4_r1.setTextureOffset(6, 38).addBox(26.7644F, -38.6268F, -22.7285F, 1.0F, 1.0F, 5.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-2.4443F, -5.4751F, -3.533F);
		rgtAntler1.addChild(cube_r5);
		setRotationAngle(cube_r5, -1.85F, 0.0F, 0.733F);

		cube_r5_r1 = new ModelRenderer(this);
		cube_r5_r1.setRotationPoint(22.4704F, 8.0711F, 17.5274F);
		cube_r5.addChild(cube_r5_r1);
		setRotationAngle(cube_r5_r1, -1.3333F, 0.7079F, 1.1572F);
		cube_r5_r1.setTextureOffset(7, 39).addBox(-18.4782F, 43.4137F, -12.3177F, 1.0F, 1.0F, 4.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(0.0F, 0.0F, 0.0F);
		rgtAntler1.addChild(cube_r6);
		setRotationAngle(cube_r6, 1.9548F, 0.0F, -0.192F);

		cube_r6_r1 = new ModelRenderer(this);
		cube_r6_r1.setRotationPoint(0.4895F, -22.7799F, -13.3435F);
		cube_r6.addChild(cube_r6_r1);
		setRotationAngle(cube_r6_r1, -2.0481F, 0.2839F, 0.2003F);
		cube_r6_r1.setTextureOffset(0, 32).addBox(8.0355F, -25.715F, 32.8895F, 1.0F, 1.0F, 11.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(ReindeerEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		this.neck_r1.rotateAngleX = headPitch * ((float) Math.PI / 180F);
		this.neck_r1.rotateAngleX = this.headRotationAngleX - (float)(Math.PI / 180F * 50.F);
		this.rgtRearLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount;
		this.lftHindLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 0.75F * limbSwingAmount;
		this.rgtFrontLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 0.75F * limbSwingAmount;
		this.lftFrontLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount;
	}

	public void setLivingAnimations(ReindeerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTick) {
		super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);
		this.headRotationAngleX = entityIn.getHeadRotationAngleX(partialTick);
		this.lftAntler1.showModel = !entityIn.isChild();
		this.rgtAntler1.showModel = !entityIn.isChild();
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		this.body.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
