package no.nla.nlacritters.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import no.nla.nlacritters.entity.ScorpionEntity;

public class ScorpionModel extends SegmentedModel<ScorpionEntity> {

	private final ModelRenderer body;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer tail0;
	private final ModelRenderer tail1;
	private final ModelRenderer tail2;
	private final ModelRenderer tail3;
	private final ModelRenderer stingerBase;
	private final ModelRenderer cube_r7;
	private final ModelRenderer leg1;
	private final ModelRenderer leg2;
	private final ModelRenderer leg3;
	private final ModelRenderer leg4;
	private final ModelRenderer leg5;
	private final ModelRenderer leg6;
	private final ModelRenderer leg7;
	private final ModelRenderer leg8;

	public ScorpionModel() {
		textureWidth = 64;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 24.0F, 0.0F);
		body.setTextureOffset(0, 11).addBox(-5.0F, -12.0F, -8.0F, 10.0F, 6.0F, 10.0F, 0.0F, false);
		body.setTextureOffset(0, 11).addBox(-5.0F, -12.0F, 2.0F, 10.0F, 6.0F, 10.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(4.0F, -7.5F, -15.0F);
		body.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, -1.2654F, 0.0F);
		cube_r1.setTextureOffset(0, 38).addBox(-6.0F, 0.0F, -1.0F, 12.0F, 1.0F, 1.0F, 0.0F, true);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-4.0F, -7.5F, -15.0F);
		body.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, 1.2654F, 0.0F);
		cube_r2.setTextureOffset(0, 38).addBox(-6.0F, 0.0F, -1.0F, 12.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(8.0F, -7.0F, -15.0F);
		body.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.0F, -1.9199F, 0.0F);
		cube_r3.setTextureOffset(0, 37).addBox(-6.0F, -1.0F, -1.0F, 12.0F, 2.0F, 2.0F, 0.0F, true);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(-8.0F, -7.0F, -15.0F);
		body.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 1.9199F, 0.0F);
		cube_r4.setTextureOffset(0, 37).addBox(-6.0F, -1.0F, -1.0F, 12.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-1.0F, -6.0F, -1.0F);
		body.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, 0.9163F, 0.0F);
		cube_r5.setTextureOffset(0, 37).addBox(0.0F, -2.0F, -1.0F, 12.0F, 2.0F, 2.0F, 0.0F, true);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(1.0F, -6.0F, -1.0F);
		body.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.0F, -0.9163F, 0.0F);
		cube_r6.setTextureOffset(0, 37).addBox(-12.0F, -2.0F, -1.0F, 12.0F, 2.0F, 2.0F, 0.0F, false);

		tail0 = new ModelRenderer(this);
		tail0.setRotationPoint(0.0F, -9.0F, 12.0F);
		body.addChild(tail0);
		setRotationAngle(tail0, 0.2618F, 0.0F, 0.0F);
		tail0.setTextureOffset(0, 27).addBox(-4.0F, -2.0F, 0.0F, 8.0F, 4.0F, 10.0F, 0.0F, false);

		tail1 = new ModelRenderer(this);
		tail1.setRotationPoint(0.0F, 0.0F, 10.0F);
		tail0.addChild(tail1);
		setRotationAngle(tail1, 0.7854F, 0.0F, 0.0F);
		tail1.setTextureOffset(40, 11).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);

		tail2 = new ModelRenderer(this);
		tail2.setRotationPoint(0.0F, 0.0F, 8.0F);
		tail1.addChild(tail2);
		setRotationAngle(tail2, 0.7854F, 0.0F, 0.0F);
		tail2.setTextureOffset(40, 11).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);

		tail3 = new ModelRenderer(this);
		tail3.setRotationPoint(0.0F, 0.0F, 8.0F);
		tail2.addChild(tail3);
		setRotationAngle(tail3, 0.9599F, 0.0F, 0.0F);
		tail3.setTextureOffset(40, 22).addBox(-1.5F, -2.0F, 0.0F, 3.0F, 3.0F, 8.0F, 0.0F, false);

		stingerBase = new ModelRenderer(this);
		stingerBase.setRotationPoint(0.0F, -0.5F, 8.0F);
		tail3.addChild(stingerBase);
		setRotationAngle(stingerBase, 0.5411F, 0.0F, 0.0F);
		stingerBase.setTextureOffset(40, 33).addBox(-3.0F, -4.0F, 0.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(0.0F, -1.9F, 5.8F);
		stingerBase.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.5672F, 0.0F, 0.0F);
		cube_r7.setTextureOffset(41, 45).addBox(-0.5F, -1.0F, 0.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);

		this.leg1 = new ModelRenderer(this, 0, 42);
		this.leg1.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg1.setRotationPoint(-4.0F, 15.0F, 2.0F);
		this.leg2 = new ModelRenderer(this, 0, 42);
		this.leg2.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg2.setRotationPoint(4.0F, 15.0F, 2.0F);
		this.leg3 = new ModelRenderer(this, 0, 42);
		this.leg3.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg3.setRotationPoint(-4.0F, 15.0F, 1.0F);
		this.leg4 = new ModelRenderer(this, 0, 42);
		this.leg4.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg4.setRotationPoint(4.0F, 15.0F, 1.0F);
		this.leg5 = new ModelRenderer(this, 0, 42);
		this.leg5.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg5.setRotationPoint(-4.0F, 15.0F, 0.0F);
		this.leg6 = new ModelRenderer(this, 0, 42);
		this.leg6.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg6.setRotationPoint(4.0F, 15.0F, 0.0F);
		this.leg7 = new ModelRenderer(this, 0, 42);
		this.leg7.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg7.setRotationPoint(-4.0F, 15.0F, -1.0F);
		this.leg8 = new ModelRenderer(this, 0, 42);
		this.leg8.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
		this.leg8.setRotationPoint(4.0F, 15.0F, -1.0F);
	}

	@Override
	public void setRotationAngles(ScorpionEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {

		this.leg1.rotateAngleZ = (-(float) Math.PI / 4F);
		this.leg2.rotateAngleZ = ((float) Math.PI / 4F);
		this.leg3.rotateAngleZ = -0.58119464F;
		this.leg4.rotateAngleZ = 0.58119464F;
		this.leg5.rotateAngleZ = -0.58119464F;
		this.leg6.rotateAngleZ = 0.58119464F;
		this.leg7.rotateAngleZ = (-(float) Math.PI / 4F);
		this.leg8.rotateAngleZ = ((float) Math.PI / 4F);
		this.leg1.rotateAngleY = ((float) Math.PI / 4F);
		this.leg2.rotateAngleY = (-(float) Math.PI / 4F);
		this.leg3.rotateAngleY = ((float) Math.PI / 8F);
		this.leg4.rotateAngleY = (-(float) Math.PI / 8F);
		this.leg5.rotateAngleY = (-(float) Math.PI / 8F);
		this.leg6.rotateAngleY = ((float) Math.PI / 8F);
		this.leg7.rotateAngleY = (-(float) Math.PI / 4F);
		this.leg8.rotateAngleY = ((float) Math.PI / 4F);

		float f3 = -(MathHelper.cos(limbSwing * 0.6662F * 2.0F + 0.0F) * 0.4F) * limbSwingAmount;
		float f4 = -(MathHelper.cos(limbSwing * 0.6662F * 2.0F + (float) Math.PI) * 0.4F) * limbSwingAmount;
		float f5 = -(MathHelper.cos(limbSwing * 0.6662F * 2.0F + ((float) Math.PI / 2F)) * 0.4F) * limbSwingAmount;
		float f6 = -(MathHelper.cos(limbSwing * 0.6662F * 2.0F + ((float) Math.PI * 1.5F)) * 0.4F) * limbSwingAmount;
		float f7 = Math.abs(MathHelper.sin(limbSwing * 0.6662F + 0.0F) * 0.4F) * limbSwingAmount;
		float f8 = Math.abs(MathHelper.sin(limbSwing * 0.6662F + (float) Math.PI) * 0.4F) * limbSwingAmount;
		float f9 = Math.abs(MathHelper.sin(limbSwing * 0.6662F + ((float) Math.PI / 2F)) * 0.4F) * limbSwingAmount;
		float f10 = Math.abs(MathHelper.sin(limbSwing * 0.6662F + ((float) Math.PI * 1.5F)) * 0.4F) * limbSwingAmount;
		this.leg1.rotateAngleY += f3;
		this.leg2.rotateAngleY += -f3;
		this.leg3.rotateAngleY += f4;
		this.leg4.rotateAngleY += -f4;
		this.leg5.rotateAngleY += f5;
		this.leg6.rotateAngleY += -f5;
		this.leg7.rotateAngleY += f6;
		this.leg8.rotateAngleY += -f6;
		this.leg1.rotateAngleZ += f7;
		this.leg2.rotateAngleZ += -f7;
		this.leg3.rotateAngleZ += f8;
		this.leg4.rotateAngleZ += -f8;
		this.leg5.rotateAngleZ += f9;
		this.leg6.rotateAngleZ += -f9;
		this.leg7.rotateAngleZ += f10;
		this.leg8.rotateAngleZ += -f10;
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		super.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setLivingAnimations(ScorpionEntity entityIn, float limbSwing, float limbSwingAmount,
			float partialTick) {
		super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);

		float gx = this.swingProgress * (float) Math.PI;
		float baseRotation1 = MathHelper.sin(gx);
		int tailSwingModifier = entityIn.getTailSwingDirection();

		RotationUtil.rotateDegrees(this.tail0, 15 + (38 * baseRotation1), 0, baseRotation1 * 30.F * tailSwingModifier);
		RotationUtil.rotateDegrees(this.tail1, 45 + (45 * baseRotation1), 0, 0);
		RotationUtil.rotateDegrees(this.tail2, 45 + (-15 * baseRotation1), 0, 0);
		RotationUtil.rotateDegrees(this.tail3, 45 + (-35 * baseRotation1), 0, 0);
		RotationUtil.rotateDegrees(this.stingerBase, 45 + (-35 * baseRotation1), 0, 0);

	}

	@Override
	public Iterable<ModelRenderer> getParts() {
		return ImmutableList.of(this.body, this.leg1, this.leg2, this.leg3, this.leg4, this.leg5, this.leg6, this.leg7,
				this.leg8);
	}
}
