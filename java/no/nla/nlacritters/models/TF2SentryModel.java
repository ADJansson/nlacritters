package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;

public class TF2SentryModel extends EntityModel<TF2SentryEntity>{

	private final ModelRenderer base;
	private final ModelRenderer tripod;
	private final ModelRenderer turret;
	private final ModelRenderer legs;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer toolbox;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;

	public TF2SentryModel() {
		textureWidth = 64;
		textureHeight = 32;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 16.0F, 0.0F);
		
		tripod = new ModelRenderer(this);
		tripod.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(tripod);
		tripod.setTextureOffset(33, 0).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		tripod.setTextureOffset(21, 14).addBox(-3.5F, -5.0F, -1.0F, 7.0F, 4.0F, 2.0F, 0.0F, false);

		turret = new ModelRenderer(this);
		turret.setRotationPoint(0.0F, -4.0F, 0.0F);
		tripod.addChild(turret);
		turret.setTextureOffset(0, 0).addBox(-4.0F, -2.0F, 1.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		turret.setTextureOffset(0, 12).addBox(-3.0F, -2.0F, -3.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
		turret.setTextureOffset(0, 0).addBox(-1.0F, -0.5F, -5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		toolbox = new ModelRenderer(this);
		toolbox.setRotationPoint(-0.5F, 5.5F, 0.0F);
		base.addChild(toolbox);
		
		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.5F, 0.5F, -0.5F);
		toolbox.addChild(cube_r1);
		setRotationAngle(cube_r1, 1.5708F, -0.6545F, -1.5708F);
		cube_r1.setTextureOffset(32, 17).addBox(2.5F, 5.0F, -5.5F, 5.0F, 2.0F, 11.0F, 1.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.5F, -4.5F, 0.0F);
		toolbox.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, -1.5708F, 0.0F);
		cube_r2.setTextureOffset(32, 2).addBox(-3.0F, 4.8F, -5.5F, 5.0F, 1.0F, 11.0F, 1.0F, false);

		legs = new ModelRenderer(this);
		legs.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(legs);
		
		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(2.0F, 2.5F, -0.5F);
		legs.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.7675F, 0.6484F, 1.0115F);
		cube_r3.setTextureOffset(30, 30).addBox(-1.0F, -0.5F, -0.5F, 10.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(-2.0F, 2.5F, -0.5F);
		legs.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.7675F, -0.6484F, -1.0115F);
		cube_r4.setTextureOffset(30, 30).addBox(-9.0F, -0.5F, -0.5F, 10.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(0.0F, 3.0F, 0.0F);
		legs.addChild(cube_r5);
		setRotationAngle(cube_r5, -0.6109F, 0.0F, 0.0F);
		cube_r5.setTextureOffset(0, 20).addBox(-2.0F, -1.0F, -2.0F, 4.0F, 2.0F, 10.0F, 0.0F, false);
	}
	
	@Override
	public void setRotationAngles(TF2SentryEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		if (entityIn.isReady()) {
			this.tripod.rotationPointY = 0.F;
			this.turret.rotateAngleX = headPitch * ((float) Math.PI / 180F);
			this.tripod.rotateAngleY = netHeadYaw * ((float) Math.PI / 180F);
			this.legs.showModel = true;
			this.toolbox.showModel = false;
		} else {
			// play building animation
			this.tripod.rotationPointY = entityIn.tripodBuildOffsetY;
			this.tripod.rotateAngleY = entityIn.tripodBuildRotation;
			this.turret.rotateAngleX = entityIn.turretBuildRotation;
			this.legs.showModel = false;
			this.toolbox.showModel = true;
		}
	}

	
	public void setLivingAnimations(TF2SentryEntity entityIn, float limbSwing, float limbSwingAmount, float partialTick) {
		super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);
	}
	
	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		this.base.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}
	
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
