package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import no.nla.nlacritters.entity.GiraffeEntity;

public class GiraffeModel extends EntityModel<GiraffeEntity>{

	public final ModelRenderer body;
	public final ModelRenderer frontLeftLeg;
	public final ModelRenderer rearLeftLeg;
	public final ModelRenderer frontRightLeg;
	public final ModelRenderer rearRightLeg;
	public final ModelRenderer tail;
	public final ModelRenderer tailTuft_r1;
	public final ModelRenderer tail_r1;
	public final ModelRenderer neck;
	public final ModelRenderer neck_r1;
	public final ModelRenderer head;
	public final ModelRenderer leftHorn_r1;
	public final ModelRenderer rightEar_r1;
	public final ModelRenderer leftEar_r1;
	public final ModelRenderer muzzle_r1;
	public final ModelRenderer head_r1;
	
	public GiraffeModel() {
		textureWidth = 64;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 24.0F, 0.0F);
		body.setTextureOffset(0, 12).addBox(-5.5F, -36.0F, -8.0F, 11.0F, 10.0F, 18.0F, 0.0F, false);

		frontLeftLeg = new ModelRenderer(this);
		frontLeftLeg.setRotationPoint(4.0F, -26.0F, -6.5F);
		body.addChild(frontLeftLeg);
		frontLeftLeg.setTextureOffset(1, 1).addBox(-1.5F, 0.0F, -1.5F, 3.0F, 26.0F, 3.0F, 0.0F, true);

		rearLeftLeg = new ModelRenderer(this);
		rearLeftLeg.setRotationPoint(4.0F, -26.0F, 7.5F);
		body.addChild(rearLeftLeg);
		rearLeftLeg.setTextureOffset(1, 1).addBox(-1.5F, 0.0F, -1.5F, 3.0F, 26.0F, 3.0F, 0.0F, true);

		frontRightLeg = new ModelRenderer(this);
		frontRightLeg.setRotationPoint(-3.0F, -26.0F, -6.5F);
		body.addChild(frontRightLeg);
		frontRightLeg.setTextureOffset(1, 1).addBox(-2.5F, 0.0F, -1.5F, 3.0F, 26.0F, 3.0F, 0.0F, false);

		rearRightLeg = new ModelRenderer(this);
		rearRightLeg.setRotationPoint(-3.0F, -26.0F, 7.5F);
		body.addChild(rearRightLeg);
		rearRightLeg.setTextureOffset(1, 1).addBox(-2.5F, 0.0F, -1.5F, 3.0F, 26.0F, 3.0F, 0.0F, false);

		tail = new ModelRenderer(this);
		tail.setRotationPoint(0.0F, -36.0F, 10.0F);
		body.addChild(tail);
		setRotationAngle(tail, 0.0436F, 0.0F, 0.0F);
		

		tailTuft_r1 = new ModelRenderer(this);
		tailTuft_r1.setRotationPoint(1.0F, 20.0F, 6.75F);
		tail.addChild(tailTuft_r1);
		setRotationAngle(tailTuft_r1, 0.3491F, 0.0F, 0.0F);
		tailTuft_r1.setTextureOffset(31, 0).addBox(-2.0F, -3.75F, 0.0068F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		tail_r1 = new ModelRenderer(this);
		tail_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		tail.addChild(tail_r1);
		setRotationAngle(tail_r1, 0.3491F, 0.0F, 0.0F);
		tail_r1.setTextureOffset(43, 0).addBox(-0.5F, 0.0F, 0.0F, 1.0F, 18.0F, 1.0F, 0.0F, false);

		neck = new ModelRenderer(this);
		neck.setRotationPoint(0.0F, -37.0F, -8.0F);
		body.addChild(neck);
		

		neck_r1 = new ModelRenderer(this);
		neck_r1.setRotationPoint(0.0F, 2.0F, 4.0F);
		neck.addChild(neck_r1);
		setRotationAngle(neck_r1, 0.3054F, 0.0F, 0.0F);
		neck_r1.setTextureOffset(48, 0).addBox(-2.0F, -26.0F, -3.0F, 4.0F, 26.0F, 4.0F, 0.0F, false);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, -23.0F, -6.0F);
		neck.addChild(head);
		

		leftHorn_r1 = new ModelRenderer(this);
		leftHorn_r1.setRotationPoint(-0.75F, -4.0F, 1.0F);
		head.addChild(leftHorn_r1);
		setRotationAngle(leftHorn_r1, -0.6109F, 0.0F, 0.0F);
		leftHorn_r1.setTextureOffset(26, 4).addBox(-1.0F, -3.0F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		leftHorn_r1.setTextureOffset(26, 4).addBox(1.5F, -3.0F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		rightEar_r1 = new ModelRenderer(this);
		rightEar_r1.setRotationPoint(-1.0F, -3.0F, 0.0F);
		head.addChild(rightEar_r1);
		setRotationAngle(rightEar_r1, 0.0F, 0.0F, -0.9599F);
		rightEar_r1.setTextureOffset(26, 49).addBox(-1.0F, -4.0F, 0.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

		leftEar_r1 = new ModelRenderer(this);
		leftEar_r1.setRotationPoint(1.0F, -3.0F, 0.0F);
		head.addChild(leftEar_r1);
		setRotationAngle(leftEar_r1, 0.0F, 0.0F, 0.9599F);
		leftEar_r1.setTextureOffset(26, 49).addBox(-1.0F, -4.0F, 0.0F, 2.0F, 4.0F, 1.0F, 0.0F, true);

		muzzle_r1 = new ModelRenderer(this);
		muzzle_r1.setRotationPoint(0.0F, 0.5F, -5.5F);
		head.addChild(muzzle_r1);
		setRotationAngle(muzzle_r1, 0.2618F, 0.0F, 0.0F);
		muzzle_r1.setTextureOffset(34, 51).addBox(-2.0F, -1.0F, -5.0F, 4.0F, 3.0F, 6.0F, 0.0F, false);

		head_r1 = new ModelRenderer(this);
		head_r1.setRotationPoint(-1.0F, 0.5F, 2.5F);
		head.addChild(head_r1);
		setRotationAngle(head_r1, 0.2618F, 0.0F, 0.0F);
		head_r1.setTextureOffset(8, 46).addBox(-1.0F, -5.0F, -7.0F, 4.0F, 5.0F, 8.0F, 0.0F, false);
	}
	
	@Override
	public void setRotationAngles(GiraffeEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		
	      this.head.rotateAngleX = headPitch * ((float)Math.PI / 180F);
	      this.head.rotateAngleY = netHeadYaw * ((float)Math.PI / 180F);
	      this.neck.rotateAngleX = this.head.rotateAngleX * 0.5F;
	      this.neck.rotateAngleY = this.head.rotateAngleY * 0.5f;
	      this.rearRightLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount;
	      this.rearLeftLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 0.75F * limbSwingAmount;
	      this.frontRightLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 0.75F * limbSwingAmount;
	      this.frontLeftLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount;
	      this.tail.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
	      this.tail.rotateAngleZ = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {
		this.body.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}
	
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
