package no.nla.nlacritters.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import no.nla.nlacritters.entity.GorillaEntity;

public class GorillaModel extends EntityModel<GorillaEntity> {

	private final ModelRenderer body;
	private final ModelRenderer rgtArm;
	private final ModelRenderer hand_r1;
	private final ModelRenderer lftArm;
	private final ModelRenderer hand_r2;
	private final ModelRenderer rgtLeg;
	private final ModelRenderer lftLeg;
	private final ModelRenderer head;
	private final ModelRenderer top_r1;
	private final ModelRenderer jaw_r1;

	public GorillaModel() {
		textureWidth = 64;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 18.0F, 0.0F);
		setRotationAngle(body, 0.6981F, 0.0F, 0.0F);
		body.setTextureOffset(0, 14).addBox(-4.0F, -10.0F, -3.0F, 8.0F, 10.0F, 6.0F, 0.0F, false);
		body.setTextureOffset(1, 31).addBox(-1.5F, -0.0924F, -1.9434F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		rgtArm = new ModelRenderer(this);
		rgtArm.setRotationPoint(-4.0F, -8.0F, 0.0F);
		body.addChild(rgtArm);

		hand_r1 = new ModelRenderer(this);
		hand_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rgtArm.addChild(hand_r1);
		setRotationAngle(hand_r1, -0.6981F, 0.0F, 0.0F);
		hand_r1.setTextureOffset(46, 14).addBox(-4.0F, 10.25F, -2.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		hand_r1.setTextureOffset(28, 0).addBox(-4.0F, 4.0F, -3.0F, 4.0F, 7.0F, 5.0F, 0.0F, false);
		hand_r1.setTextureOffset(30, 2).addBox(-3.0F, -1.0F, -2.0F, 3.0F, 5.0F, 3.0F, 0.0F, false);

		lftArm = new ModelRenderer(this);
		lftArm.setRotationPoint(4.0F, -8.0F, 0.0F);
		body.addChild(lftArm);

		hand_r2 = new ModelRenderer(this);
		hand_r2.setRotationPoint(4.0F, 0.0F, 0.0F);
		lftArm.addChild(hand_r2);
		setRotationAngle(hand_r2, -0.6981F, 0.0F, 0.0F);
		hand_r2.setTextureOffset(46, 14).addBox(-4.0F, 10.25F, -2.0F, 4.0F, 2.0F, 4.0F, 0.0F, true);
		hand_r2.setTextureOffset(28, 0).addBox(-4.0F, 4.0F, -3.0F, 4.0F, 7.0F, 5.0F, 0.0F, true);
		hand_r2.setTextureOffset(30, 2).addBox(-4.0F, -1.0F, -2.0F, 3.0F, 5.0F, 3.0F, 0.0F, true);

		rgtLeg = new ModelRenderer(this);
		rgtLeg.setRotationPoint(-5.0F, 2.2465F, -0.8177F);
		body.addChild(rgtLeg);
		setRotationAngle(rgtLeg, -0.6981F, 0.0F, 0.0F);
		rgtLeg.setTextureOffset(28, 20).addBox(0.0F, -1.0F, -2.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);

		lftLeg = new ModelRenderer(this);
		lftLeg.setRotationPoint(5.0F, 2.2465F, -0.8177F);
		body.addChild(lftLeg);
		setRotationAngle(lftLeg, -0.6981F, 0.0F, 0.0F);
		lftLeg.setTextureOffset(28, 20).addBox(-4.0F, -1.0F, -2.0F, 4.0F, 5.0F, 4.0F, 0.0F, true);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, -10.0F, -1.0F);
		body.addChild(head);

		top_r1 = new ModelRenderer(this);
		top_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.addChild(top_r1);
		setRotationAngle(top_r1, -0.6981F, 0.0F, 0.0F);
		top_r1.setTextureOffset(1, 1).addBox(-2.5F, -7.6308F, -1.949F, 5.0F, 4.0F, 5.0F, 0.0F, false);
		top_r1.setTextureOffset(0, 2).addBox(-3.0F, -5.0F, -3.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);

		jaw_r1 = new ModelRenderer(this);
		jaw_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.addChild(jaw_r1);
		setRotationAngle(jaw_r1, -0.9599F, 0.0F, 0.0F);
		jaw_r1.setTextureOffset(18, 30).addBox(-2.0F, -1.119F, -3.3672F, 4.0F, 4.0F, 3.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(GorillaEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {

		this.head.rotateAngleX = headPitch * ((float) Math.PI / 180F);
		this.head.rotateAngleY = netHeadYaw * ((float) Math.PI / 180F) * 0.5F;

		if (entityIn.isOnLadder()) {
			this.body.rotateAngleX = 0.F;
			RotationUtil.rotateDegrees(this.rgtArm, -70.F, 0.F, 0.F);
			RotationUtil.rotateDegrees(this.lftArm, -70.F, 0.F, 0.F);
		} else {
			this.body.rotateAngleX = ((float) Math.PI / 180F * 40);
			this.rgtArm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 0.75F * limbSwingAmount;
			this.lftArm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount;
		}

		this.rgtLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.75F * limbSwingAmount
				- ((float) Math.PI / 180F * 40);
		this.lftLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 0.75F * limbSwingAmount
				- ((float) Math.PI / 180F * 40);

	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
			float red, float green, float blue, float alpha) {

		this.body.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}

	public void setRotationAngle(ModelRenderer ModelRenderer, float x, float y, float z) {
		ModelRenderer.rotateAngleX = x;
		ModelRenderer.rotateAngleY = y;
		ModelRenderer.rotateAngleZ = z;
	}
}
