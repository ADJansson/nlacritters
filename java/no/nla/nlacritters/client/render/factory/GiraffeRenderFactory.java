package no.nla.nlacritters.client.render.factory;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.GiraffeRenderer;
import no.nla.nlacritters.entity.GiraffeEntity;
import no.nla.nlacritters.models.GiraffeModel;

public class GiraffeRenderFactory <T extends GiraffeEntity> implements IRenderFactory<T> {
	
	public static final GiraffeRenderFactory<GiraffeEntity> INSTANCE = new GiraffeRenderFactory<GiraffeEntity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new GiraffeRenderer(manager, new GiraffeModel());
	}
}
