package no.nla.nlacritters.client.render.factory;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.ScorpionRenderer;
import no.nla.nlacritters.entity.ScorpionEntity;
import no.nla.nlacritters.models.ScorpionModel;

public class ScorpionRenderFactory<T extends ScorpionEntity> implements IRenderFactory<T> {
	
	public static final ScorpionRenderFactory<ScorpionEntity> INSTANCE = new ScorpionRenderFactory<ScorpionEntity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new ScorpionRenderer(manager, new ScorpionModel());
	}
}
