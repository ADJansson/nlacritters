package no.nla.nlacritters.client.render.factory;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.OrangutanRenderer;
import no.nla.nlacritters.entity.OrangutanEntity;
import no.nla.nlacritters.models.OrangutanModel;

public class OrangutanRenderFactory <T extends OrangutanEntity> implements IRenderFactory<T> {
	
	public static final OrangutanRenderFactory<OrangutanEntity> INSTANCE = new OrangutanRenderFactory<OrangutanEntity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new OrangutanRenderer(manager, new OrangutanModel());
	}
}
