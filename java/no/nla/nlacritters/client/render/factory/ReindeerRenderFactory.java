package no.nla.nlacritters.client.render.factory;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.ReindeerRenderer;
import no.nla.nlacritters.entity.ReindeerEntity;
import no.nla.nlacritters.models.ReindeerModel;

public class ReindeerRenderFactory <T extends ReindeerEntity> implements IRenderFactory<T> {
	
	public static final ReindeerRenderFactory<ReindeerEntity> INSTANCE = new ReindeerRenderFactory<ReindeerEntity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new ReindeerRenderer(manager, new ReindeerModel());
	}
}
