package no.nla.nlacritters.client.render.factory.tf2sentry;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.tf2sentry.TF2SentryRenderer;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;
import no.nla.nlacritters.models.TF2SentryModel;

public class TF2SentryRenderFactory <T extends TF2SentryEntity> implements IRenderFactory<T> {

	public static final TF2SentryRenderFactory<TF2SentryEntity> INSTANCE = new TF2SentryRenderFactory<TF2SentryEntity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new TF2SentryRenderer(manager, new TF2SentryModel());
	}
}
