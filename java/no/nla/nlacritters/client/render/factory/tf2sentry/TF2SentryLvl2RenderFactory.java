package no.nla.nlacritters.client.render.factory.tf2sentry;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.tf2sentry.TF2SentryLvl2Renderer;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryLvl2Entity;
import no.nla.nlacritters.models.TF2SentryLvl2Model;

public class TF2SentryLvl2RenderFactory <T extends TF2SentryLvl2Entity> implements IRenderFactory<T> {

	public static final TF2SentryLvl2RenderFactory<TF2SentryLvl2Entity> INSTANCE = new TF2SentryLvl2RenderFactory<TF2SentryLvl2Entity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new TF2SentryLvl2Renderer(manager, new TF2SentryLvl2Model());
	}
}
