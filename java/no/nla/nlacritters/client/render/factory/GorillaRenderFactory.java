package no.nla.nlacritters.client.render.factory;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.GorillaRenderer;
import no.nla.nlacritters.entity.GorillaEntity;
import no.nla.nlacritters.models.GorillaModel;

public class GorillaRenderFactory <T extends GorillaEntity> implements IRenderFactory<T> {
	
	public static final GorillaRenderFactory<GorillaEntity> INSTANCE = new GorillaRenderFactory<GorillaEntity>();
	
	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new GorillaRenderer(manager, new GorillaModel());
	}
}
