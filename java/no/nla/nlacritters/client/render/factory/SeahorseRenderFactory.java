package no.nla.nlacritters.client.render.factory;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import no.nla.nlacritters.client.render.SeahorseRenderer;
import no.nla.nlacritters.entity.SeahorseEntity;
import no.nla.nlacritters.models.SeahorseModel;

public class SeahorseRenderFactory<T extends SeahorseEntity> implements IRenderFactory<T> {

	public static final SeahorseRenderFactory<SeahorseEntity> INSTANCE = new SeahorseRenderFactory<SeahorseEntity>();

	@Override
	public EntityRenderer<? super T> createRenderFor(EntityRendererManager manager) {
		return new SeahorseRenderer(manager, new SeahorseModel());
	}
}
