package no.nla.nlacritters.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.OrangutanEntity;
import no.nla.nlacritters.models.OrangutanModel;
import no.nla.nlacritters.util.ModConstants;

public class OrangutanRenderer extends MobRenderer<OrangutanEntity, OrangutanModel> {

	protected static final ResourceLocation[] SKINS = new ResourceLocation[ModConstants.GIRAFFE_SKINS];
	
	public OrangutanRenderer(EntityRendererManager renderManagerIn, OrangutanModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 0.7F);
		
		for (int i = 0; i < ModConstants.ORANGUTAN_SKINS; i++) {
			SKINS[i] = new ResourceLocation(Main.MODID, "textures/entity/orangutan/orangutan" + i + ".png");
		}
	}

	@Override
	protected void preRenderCallback(OrangutanEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		if (entityliving.isChild()) {
			matrixStackIn.scale(0.5F, 0.5F, 0.5F);
		} else {
			matrixStackIn.scale(1.2F, 1.2F, 1.2F);
		}
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public ResourceLocation getEntityTexture(OrangutanEntity entity) {
		return SKINS[entity.getSkinIndex()];
	}
}