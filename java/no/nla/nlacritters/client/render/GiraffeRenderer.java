package no.nla.nlacritters.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.GiraffeEntity;
import no.nla.nlacritters.models.GiraffeModel;
import no.nla.nlacritters.util.ModConstants;

public class GiraffeRenderer extends MobRenderer<GiraffeEntity, GiraffeModel> {

	protected static final ResourceLocation[] SKINS = new ResourceLocation[ModConstants.GIRAFFE_SKINS];
	
	public GiraffeRenderer(EntityRendererManager renderManagerIn, GiraffeModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 1.F);
		
		for (int i = 0; i < ModConstants.GIRAFFE_SKINS; i++) {
			SKINS[i] = new ResourceLocation(Main.MODID, "textures/entity/giraffe/giraffe" + i + ".png");
		}
	}

	@Override
	protected void preRenderCallback(GiraffeEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		if (entityliving.isChild()) {
			matrixStackIn.scale(0.5F, 0.5F, 0.5F);
		}
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public ResourceLocation getEntityTexture(GiraffeEntity entity) {
		return SKINS[entity.getSkinIndex()];
	}
}
