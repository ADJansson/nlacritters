package no.nla.nlacritters.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.GorillaEntity;
import no.nla.nlacritters.models.GorillaModel;
import no.nla.nlacritters.util.ModConstants;

public class GorillaRenderer extends MobRenderer<GorillaEntity, GorillaModel> {

	protected static final ResourceLocation[] SKINS = new ResourceLocation[ModConstants.GORILLA_SKINS];
	
	public GorillaRenderer(EntityRendererManager renderManagerIn, GorillaModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 1.F);
		
		for (int i = 0; i < ModConstants.GORILLA_SKINS; i++) {
			SKINS[i] = new ResourceLocation(Main.MODID, "textures/entity/gorilla/gorilla" + i + ".png");
		}
	}

	@Override
	protected void preRenderCallback(GorillaEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		if (entityliving.isChild()) {
			matrixStackIn.scale(0.5F, 0.5F, 0.5F);
		} else if (entityliving.isSilverback()) {
			matrixStackIn.scale(1.5F, 1.5F, 1.5F);
		} else {
			matrixStackIn.scale(1.2F, 1.2F, 1.2F);
		}
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public ResourceLocation getEntityTexture(GorillaEntity entity) {
		return SKINS[entity.getSkinIndex()];
	}
}
