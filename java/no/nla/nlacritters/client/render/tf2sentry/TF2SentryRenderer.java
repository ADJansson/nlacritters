package no.nla.nlacritters.client.render.tf2sentry;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;
import no.nla.nlacritters.models.TF2SentryModel;

public class TF2SentryRenderer extends MobRenderer<TF2SentryEntity, TF2SentryModel> {

	protected static ResourceLocation RED_SKIN;
	protected static ResourceLocation BLU_SKIN;
	
	public TF2SentryRenderer(EntityRendererManager renderManagerIn, TF2SentryModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 0.7F);
		
		RED_SKIN = new ResourceLocation(Main.MODID, "textures/entity/tf2sentry/sentry_lvl1_red.png");
		BLU_SKIN = new ResourceLocation(Main.MODID, "textures/entity/tf2sentry/sentry_lvl1_blu.png");
	}

	@Override
	protected void preRenderCallback(TF2SentryEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		matrixStackIn.scale(1.2F, 1.2F, 1.2F);
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public void render(TF2SentryEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}
	
	@Override
	public ResourceLocation getEntityTexture(TF2SentryEntity entity) {
		return entity.getTeamColor() == 0 ? RED_SKIN : BLU_SKIN;
	}
}
