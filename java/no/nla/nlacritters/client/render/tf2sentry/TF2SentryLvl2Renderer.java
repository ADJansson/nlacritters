package no.nla.nlacritters.client.render.tf2sentry;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryLvl2Entity;
import no.nla.nlacritters.models.TF2SentryLvl2Model;

public class TF2SentryLvl2Renderer extends MobRenderer<TF2SentryLvl2Entity, TF2SentryLvl2Model> {
	protected static ResourceLocation RED_SKIN;
	protected static ResourceLocation BLU_SKIN;
	
	public TF2SentryLvl2Renderer(EntityRendererManager renderManagerIn, TF2SentryLvl2Model entityModelIn) {
		super(renderManagerIn, entityModelIn, 0.7F);
		
		RED_SKIN = new ResourceLocation(Main.MODID, "textures/entity/tf2sentry/sentry_lvl2_red.png");
		BLU_SKIN = new ResourceLocation(Main.MODID, "textures/entity/tf2sentry/sentry_lvl2_blu.png");
	}

	@Override
	protected void preRenderCallback(TF2SentryLvl2Entity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		matrixStackIn.scale(1.3F, 1.4F, 1.3F);
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public void render(TF2SentryLvl2Entity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}
	
	@Override
	public ResourceLocation getEntityTexture(TF2SentryLvl2Entity entity) {
		return entity.getTeamColor() == 0 ? RED_SKIN : BLU_SKIN;
	}
}
