package no.nla.nlacritters.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.SeahorseEntity;
import no.nla.nlacritters.models.SeahorseModel;
import no.nla.nlacritters.util.ModConstants;

public class SeahorseRenderer extends MobRenderer<SeahorseEntity, SeahorseModel>{

	protected static final ResourceLocation[] SKINS = new ResourceLocation[ModConstants.SEAHORSE_SKINS];
	
	public SeahorseRenderer(EntityRendererManager renderManagerIn, SeahorseModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 1.F);
		
		for (int i = 0; i < ModConstants.SEAHORSE_SKINS; i++) {
			SKINS[i] = new ResourceLocation(Main.MODID, "textures/entity/seahorse/horse" + i + ".png");
		}
	}

	@Override
	protected void preRenderCallback(SeahorseEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public ResourceLocation getEntityTexture(SeahorseEntity entity) {
		return SKINS[entity.getSkinIndex()];
	}
}
