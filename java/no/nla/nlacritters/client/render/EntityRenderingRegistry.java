package no.nla.nlacritters.client.render;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import no.nla.nlacritters.client.render.factory.GiraffeRenderFactory;
import no.nla.nlacritters.client.render.factory.GorillaRenderFactory;
import no.nla.nlacritters.client.render.factory.OrangutanRenderFactory;
import no.nla.nlacritters.client.render.factory.ReindeerRenderFactory;
import no.nla.nlacritters.client.render.factory.ScorpionRenderFactory;
import no.nla.nlacritters.client.render.factory.SeahorseRenderFactory;
import no.nla.nlacritters.client.render.factory.tf2sentry.TF2SentryLvl2RenderFactory;
import no.nla.nlacritters.client.render.factory.tf2sentry.TF2SentryRenderFactory;
import no.nla.nlacritters.entity.ModEntityType;

@OnlyIn(Dist.CLIENT)
public class EntityRenderingRegistry {

	public static void registerAll() {
		
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.GIRAFFE, GiraffeRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.GORILLA, GorillaRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.ORANGUTAN, OrangutanRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.REINDEER, ReindeerRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.SCORPION, ScorpionRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.SEAHORSE, SeahorseRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.TF2_SENTRY_LVL1, TF2SentryRenderFactory.INSTANCE);
		RenderingRegistry.registerEntityRenderingHandler(ModEntityType.TF2_SENTRY_LVL2, TF2SentryLvl2RenderFactory.INSTANCE);
	}
}
