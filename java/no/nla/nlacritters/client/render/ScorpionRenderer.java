package no.nla.nlacritters.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.ScorpionEntity;
import no.nla.nlacritters.models.ScorpionModel;
import no.nla.nlacritters.util.ModConstants;

public class ScorpionRenderer extends MobRenderer<ScorpionEntity, ScorpionModel>{

	protected static final ResourceLocation[] SKINS = new ResourceLocation[ModConstants.SCORPION_SKINS];
	
	public ScorpionRenderer(EntityRendererManager renderManagerIn, ScorpionModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 0.7F);
		
		for (int i = 0; i < ModConstants.SCORPION_SKINS; i++) {
			SKINS[i] = new ResourceLocation(Main.MODID, "textures/entity/scorpion/scorpion" + i + ".png");
		}
	}

	@Override
	protected void preRenderCallback(ScorpionEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		matrixStackIn.scale(0.7F, 0.7F, 0.7F);
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public ResourceLocation getEntityTexture(ScorpionEntity entity) {
		return SKINS[entity.getSkinIndex()];
	}
}
