package no.nla.nlacritters.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import no.nla.nlacritters.client.Main;
import no.nla.nlacritters.entity.ReindeerEntity;
import no.nla.nlacritters.models.ReindeerModel;
import no.nla.nlacritters.util.ModConstants;

public class ReindeerRenderer extends MobRenderer<ReindeerEntity, ReindeerModel>{

	protected static final ResourceLocation[] SKINS = new ResourceLocation[ModConstants.REINDEER_SKINS];
	
	public ReindeerRenderer(EntityRendererManager renderManagerIn, ReindeerModel entityModelIn) {
		super(renderManagerIn, entityModelIn, 1.F);
		
		for (int i = 0; i < ModConstants.REINDEER_SKINS; i++) {
			SKINS[i] = new ResourceLocation(Main.MODID, "textures/entity/reindeer/reindeer" + i + ".png");
		}
	}

	@Override
	protected void preRenderCallback(ReindeerEntity entityliving, MatrixStack matrixStackIn, float partialTickTime) {
		if (entityliving.isChild()) {
			matrixStackIn.scale(0.5F, 0.5F, 0.5F);
		}
		super.preRenderCallback(entityliving, matrixStackIn, partialTickTime);
	}
	
	@Override
	public ResourceLocation getEntityTexture(ReindeerEntity entity) {
		return SKINS[entity.getSkinIndex()];
	}
}
