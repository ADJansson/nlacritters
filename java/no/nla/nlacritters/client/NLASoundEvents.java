package no.nla.nlacritters.client;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public class NLASoundEvents {
	public static final SoundEvent ENTITY_GORILLA_AMBIENT = new SoundEvent(new ResourceLocation(Main.MODID, "entity_gorilla_ambient"));
	public static final SoundEvent ENTITY_ORANGUTAN_AMBIENT = new SoundEvent(new ResourceLocation(Main.MODID, "entity_orangutan_ambient"));
}
