package no.nla.nlacritters.client;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import no.nla.nlacritters.client.render.EntityRenderingRegistry;
import no.nla.nlacritters.entity.GiraffeEntity;
import no.nla.nlacritters.entity.GorillaEntity;
import no.nla.nlacritters.entity.ModEntityType;
import no.nla.nlacritters.entity.OrangutanEntity;
import no.nla.nlacritters.entity.ReindeerEntity;
import no.nla.nlacritters.entity.ScorpionEntity;
import no.nla.nlacritters.entity.SeahorseEntity;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryEntity;
import no.nla.nlacritters.entity.tf2sentry.TF2SentryLvl2Entity;
import no.nla.nlacritters.items.ModItems;

@Mod("nlacritters")
public class Main {
	
	public static final String MODID = "nlacritters";
	public static final String MODNAME = "NLA Critters";
	public static final String VERSION = "1.0.0";
	
	public static Main instance;
	
	public Main() {
		instance = this;
		
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
		
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void setup(final FMLCommonSetupEvent event)
	{		
		GlobalEntityTypeAttributes.put(ModEntityType.GIRAFFE, GiraffeEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.GORILLA, GorillaEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.ORANGUTAN, OrangutanEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.REINDEER, ReindeerEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.SCORPION, ScorpionEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.SEAHORSE, SeahorseEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.TF2_SENTRY_LVL1, TF2SentryEntity.prepareAttributes().create());
		GlobalEntityTypeAttributes.put(ModEntityType.TF2_SENTRY_LVL2, TF2SentryLvl2Entity.prepareAttributes().create());
	}
	
	private void clientRegistries(final FMLClientSetupEvent event)
	{
		EntityRenderingRegistry.registerAll();
	}
	
	@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
	public static class RegsitryEvents
	{
		@SubscribeEvent
		public static void registerEntities(final RegistryEvent.Register<EntityType<?>> event)
		{
			ModEntityType.registerEntityTypes(event);
		}
		
		@SubscribeEvent
		public static void registerItems(final RegistryEvent.Register<Item> event) {

			ModEntityType.registerEggs(event);
			ModItems.registerItems(event);
		}
	}
}
